-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 27, 2018 at 12:01 PM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 5.6.33-3+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clientdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `idadmin` int(10) UNSIGNED NOT NULL,
  `admincode` varchar(5) NOT NULL,
  `adminname` varchar(25) NOT NULL,
  `adminpswd` varchar(40) NOT NULL,
  `akses` enum('Admin','Piket') NOT NULL DEFAULT 'Piket',
  `lastlog` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`idadmin`, `admincode`, `adminname`, `adminpswd`, `akses`, `lastlog`) VALUES
(1, 'LNLRD', 'Landlord', 'b1b3773a05c0ed0176787a4f1574ff0075f7521e', 'Admin', '2018-02-27 04:31:40'),
(2, 'APYUD', 'Hanamichi', '4e17a448e043206801b95de317e07c839770c8b8', 'Piket', NULL),
(3, 'SANDI', 'Sandi', 'b1b3773a05c0ed0176787a4f1574ff0075f7521e', 'Piket', NULL),
(4, 'NAJIB', 'Najib', 'b1b3773a05c0ed0176787a4f1574ff0075f7521e', 'Admin', NULL),
(5, 'ZAKIY', 'Zaki', 'b1b3773a05c0ed0176787a4f1574ff0075f7521e', 'Admin', NULL),
(6, 'IYANY', 'Iyan', 'b1b3773a05c0ed0176787a4f1574ff0075f7521e', 'Piket', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fpmesin`
--

CREATE TABLE `fpmesin` (
  `idmesin` int(10) UNSIGNED NOT NULL,
  `mesin` varchar(25) NOT NULL,
  `ipmesin` varchar(15) NOT NULL,
  `comkey` varchar(15) NOT NULL,
  `added` timestamp NULL DEFAULT NULL,
  `addby` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fpmesin`
--

INSERT INTO `fpmesin` (`idmesin`, `mesin`, `ipmesin`, `comkey`, `added`, `addby`) VALUES
(1, 'Mesin kelas X', '192.168.1.201', '0', '2017-12-09 10:43:26', 1),
(2, 'Mesin kelas XI', '192.168.1.20', '0', '2017-12-22 08:48:45', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jari`
--

CREATE TABLE `jari` (
  `idjari` int(10) UNSIGNED NOT NULL,
  `nomor` int(10) UNSIGNED DEFAULT NULL,
  `ukuran` int(10) UNSIGNED DEFAULT NULL,
  `template` text,
  `nis` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jetconfig`
--

CREATE TABLE `jetconfig` (
  `idjet` int(10) UNSIGNED NOT NULL,
  `jetkey` varchar(15) NOT NULL,
  `jetval` text NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jetconfig`
--

INSERT INTO `jetconfig` (`idjet`, `jetkey`, `jetval`, `updated`) VALUES
(1, 'url_api', 'http://dev.jetschool.id/jetoffice/api', '2018-02-27 04:56:29'),
(2, 'sch_code', '42d395efa9f6c50154cc92d671da4321fc325265', '2018-01-01 15:09:45'),
(3, 'sch_keys', 'ce79a33ebf978d7bb5498fbdb8faa81a068fd210', '2018-01-01 15:09:45');

-- --------------------------------------------------------

--
-- Table structure for table `logfp`
--

CREATE TABLE `logfp` (
  `idlog` int(10) UNSIGNED NOT NULL,
  `nis` varchar(16) NOT NULL,
  `tipe` enum('0','1') NOT NULL DEFAULT '0',
  `waktu` timestamp NULL DEFAULT NULL,
  `sync` enum('0','1') NOT NULL DEFAULT '0',
  `mesin` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `nis` varchar(16) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `pswd` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`idadmin`);

--
-- Indexes for table `fpmesin`
--
ALTER TABLE `fpmesin`
  ADD PRIMARY KEY (`idmesin`),
  ADD KEY `idadmin` (`addby`);

--
-- Indexes for table `jari`
--
ALTER TABLE `jari`
  ADD PRIMARY KEY (`idjari`),
  ADD KEY `siswa` (`nis`);

--
-- Indexes for table `jetconfig`
--
ALTER TABLE `jetconfig`
  ADD PRIMARY KEY (`idjet`);

--
-- Indexes for table `logfp`
--
ALTER TABLE `logfp`
  ADD PRIMARY KEY (`idlog`),
  ADD KEY `nis` (`nis`),
  ADD KEY `mesin` (`mesin`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`nis`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `idadmin` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `fpmesin`
--
ALTER TABLE `fpmesin`
  MODIFY `idmesin` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `jari`
--
ALTER TABLE `jari`
  MODIFY `idjari` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jetconfig`
--
ALTER TABLE `jetconfig`
  MODIFY `idjet` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `logfp`
--
ALTER TABLE `logfp`
  MODIFY `idlog` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
ALTER TABLE `logfp` ADD `pin` VARCHAR(9) NULL DEFAULT NULL AFTER `nis`;
