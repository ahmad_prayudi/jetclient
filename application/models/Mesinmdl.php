<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mesinmdl extends CI_Model {

	public function getMachinebyIP(String $ip) {
		$this->db->where(['ipmesin' => $ip]);
		$m = $this->db->get('fpmesin')->row();
		return $m;
	}

	public function getMesinrow($id) {
		$row = $this->db->get_where('fpmesin', array('idmesin' => $id))->row();
		return $row;
	}

	public function tableMesin() {
		$res = $this->db->get('fpmesin')->result();
		$tr = "";
		$tr = "<tbody>";
		foreach ($res as $val) {
			$btn = "";
			if ($this->session->userdata('akses') == 'Admin') {
				$ulink = site_url('mesin/editfpm/'.$val->idmesin);
				$dlink = site_url('client/mesin/delete/'.$val->idmesin);
				$btn .= '<a href="'.$ulink.'" class="btn btn-sm btn-warning" title="Edit data">';
				$btn .= '<span class="fa fa-cog"></span> Edit</a>&nbsp;';
				$btn .= '<a href="'.$dlink.'" class="btn btn-sm btn-danger" title="Hapus data">';
				$btn .= '<span class="fa fa-trash"></span> Hapus</a>';
			}

			$sts = $this->clientmdl->fpConnect($val->ipmesin,$val->comkey);
			if ($sts == 1) {
				$qsts = '<div id="sts'.$val->idmesin.'"><span class="badge badge-success"></span> Online</div>';
			} else {
				$qsts = '<div id="sts'.$val->idmesin.'"><span class="badge badge-danger"></span> Offline</div>';
			}
			
			$tr .= '<tr>';
			$tr .= '<td>'.$val->mesin.'</td>';
			$tr .= '<td>'.$val->ipmesin.'</td>';
			$tr .= '<td>'.$val->comkey.'</td>';
			$tr .= '<td>'.$qsts.'</td>';
			$tr .= '<td>'.$btn.'</td>';
			$tr .= '</tr>';
		}
		$tr .= "</tbody>";
		return $tr;
	}

}
