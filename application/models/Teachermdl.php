<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Teachermdl extends CI_Model {

	public function getAllGuru() {
		return $this->db->get('guru')->result();
	}

	public function guruById($id) {
		if ($this->clientmdl->getConfig('idguru') == 'nip') {
			return $this->db->where('nip',$id)->get('guru')->row();
		} else {
			return $this->db->where('pin',$id)->get('guru')->row();
		}
	}

	public function guruByNIPOnly($nip) {
		return $this->db->where('nip',$nip)->get('guru')->row();
	}

	public function insertGuru($data) {
		if ($this->db->insert('guru',$data)) {
			return true;
		} else {
			return false;
		}
	}

	public function updateGuruByNIP($nip, $data) {
		if ($this->db->where('nip',$nip)->update('guru',$data)) {
			return true;
		} else {
			return false;
		}
	}

	public function hapusGuruByNIP($nip, $data) {
		# hapus semua data sidik jari guru
		try {
			$this->db->beginTransaction();
			$this->db->where('nip',$nip)->delete('jariguru');
			$this->db->where('nip',$nip)->delete('guru');
			$this->db->commit();
			return true;
		} catch (Exception $e) {
			$this->db->rollback();
			return false;
		}
	}

	public function logPresence() {
		$this->db->where('sync',0);
		$this->db->order_by('waktu desc, tipe asc');
		return $this->db->get('logfpguru', 150, 0)->result();
	}

	public function getFPbyNIP($nip) {
		return $this->db->where('nip',$nip)->get('jariguru')->result();
	}

	public function getpasswordfp($ip, $key, $pin, $port = '80') {
		$Connect = @fsockopen($ip, '80', $errno, $errstr, 1);
		$soap_request="<GetUserInfo><ArgComKey xsi:type=\"xsd:integer\">".$key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">".$pin."</PIN></Arg></GetUserInfo>";
		$newLine="\r\n";
		@fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
	    @fputs($Connect, "Content-Type: text/xml".$newLine);
	    @fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
	    @fputs($Connect, $soap_request.$newLine);
		$buffer="";
		while($Response=fgets($Connect, 1024)){
			$buffer=$buffer.$Response;
		}
		$buffer = $this->clientmdl->parser($buffer,"<GetUserInfoResponse>","</GetUserInfoResponse>");
		$buffer=explode("\r\n",$buffer);

		if ($pin != '') {
			$guru = $this->guruById($pin);
			
			$nip = $guru->nip;
			if (count($buffer) >= 3) {
				for($a = 0; $a < count($buffer); $a++) {
					$data = $this->clientmdl->parser($buffer[$a],"<Row>","</Row>");
					$pin = $this->clientmdl->parser($data,"<PIN>","</PIN>");
					$pswd = $this->clientmdl->parser($data,"<Password>","</Password>");
					if ($pin != '') {
						$this->db->where(array('nip' => $nip));
						$this->db->update('guru', array('pswd' => $pswd));
						//echo "$nip dgn password $pswd selesai <br>";
						break;
					}
				}
			}
		}
	}

	public function gettemplatefp($ip, $key, $pin, $port = '80') {
		$Connect = @fsockopen($ip, '80', $errno, $errstr, 1);
		$soap_request = "<GetUserTemplate><ArgComKey xsi:type=\"xsd:integer\">".$key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">".$pin."</PIN><FingerID xsi:type=\"xsd:integer\"></FingerID></Arg></GetUserTemplate>";
		$newLine="\r\n";
		@fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
	    @fputs($Connect, "Content-Type: text/xml".$newLine);
	    @fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
	    @fputs($Connect, $soap_request.$newLine);
		$buffer="";
		while($Response=fgets($Connect, 1024)){
			$buffer=$buffer.$Response;
		}
		$buffer = $this->clientmdl->parser($buffer,"<GetUserTemplateResponse>","</GetUserTemplateResponse>");
		$buffer=explode("\r\n",$buffer);

		# ambil data gurunya
		if ($pin != '') {
			$guru = $this->guruById($pin);
			$nip = $guru->nip;
			if (count($buffer) >= 3) {
				for($a = 0; $a < count($buffer); $a++) {
					$data = $this->clientmdl->parser($buffer[$a],"<Row>","</Row>");
					$pin = $this->clientmdl->parser($data,"<PIN>","</PIN>");
					if ($pin != '') {
						$fid = $this->clientmdl->parser($data,"<FingerID>","</FingerID>");
						$siz = $this->clientmdl->parser($data,"<Size>","</Size>");
						$tpl = $this->clientmdl->parser($data,"<Template>","</Template>");
						$jari = $this->db->get_where('jariguru', array('nip' => $nip, 'nomor' => $fid));
						if ($jari->num_rows() == 0) {
							$data = array('nip' => $nip, 'nomor' => $fid, 'ukuran' => $siz, 'template' => $tpl);
							$this->db->insert('jariguru', $data);
							//echo "$nip insert template $tpl<br>";
						} else {
							$data = array('ukuran' => $siz, 'template' => $tpl);
							$wh = array('nip' => $nip, 'nomor' => $fid);
							$this->db->where($wh);
							$this->db->update('jariguru', $data);
							//echo "$nip update template $tpl<br>";
						}
					}
				}
			}
		}
	}

	public function setusertemplatefp($ip, $key, $pin, $port = '80') {
		$guru = $this->guruById($pin);
		if ($guru) {
        	$nip = $guru->nip;
        } else {
        	return false;
        }
			
		# get finger database
		$fp = $this->getFPbyNIP($nip);
		if (count($fp) > 0) {
			foreach ($fp as $f) {
				mulai:
				$Connect = @fsockopen($ip, '80', $errno, $errstr, 1);
				if ($Connect) {
					$fn = $f->nomor;
					$temp = $f->template;
					$soap_request="<SetUserTemplate><ArgComKey xsi:type=\"xsd:integer\">".$key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">".$pin."</PIN><FingerID xsi:type=\"xsd:integer\">".$fn."</FingerID><Size>".strlen($temp)."</Size><Valid>1</Valid><Template>".$temp."</Template></Arg></SetUserTemplate>";
					$newLine="\r\n";
					@fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
					@fputs($Connect, "Content-Type: text/xml".$newLine);
					@fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
					@fputs($Connect, $soap_request.$newLine);
					$buffer="";
					while($Response=fgets($Connect, 1024)){
						$buffer=$buffer.$Response;
					}
					$buffer=$this->clientmdl->parser($buffer,"<SetUserTemplateResponse>","</SetUserTemplateResponse>");
					$buffer=$this->clientmdl->parser($buffer,"<Information>","</Information>");
					$this->clientmdl->refreshMechine($ip, $key);
					echo $pin." ".$fn." sukses<br>";
				} else {
					echo "$pin tidak bisa terhubung ke mesin<br>";
				}
			}
			#return true;
			echo $pin." ".$fn." sukses<br>";
		}
		#return false;
		echo $pin." tidak punya jari";
    }

	public function synchronize() {
		$api = $this->clientmdl->getConfig('url_api');
		$api = strtolower($api);
		$id = $this->clientmdl->getConfig('sch_keys');
		$expl = explode('/api',strtolower($api));
		$apis = $expl[0].'/services/downloadTeacher/'.$id;
		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, $apis); 
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$tmp = curl_exec ($ch);
		curl_close ($ch);
		$tempe = json_decode($tmp);
		if (count($tempe) > 0) {
			$exe = $this->importGuru($tempe);
		} else {
			$this->session->set_flashdata('msg','Tidak dapat terhubung ke server '.$apis.'.');
			//$this->session->set_flashdata('msg',$expl);
			$exe = 'no_connection';
		}

		return $tempe;
	}

	public function createPIN() {
		$year = date('y');
		$this->db->select_max('pin');
		$this->db->where('MID(pin,2,2)',$year);
		$r = $this->db->get('guru');
		if ($r->num_rows() > 0) {
			$no = $r->row()->pin;
			$sno = substr($no,3,3);
			$ino = intval($sno) + 1;
			$num = str_pad($ino, 3, '0', STR_PAD_LEFT);
			$pin = '9'.$year.$num;
		} else {
			$pin = '9'.$year.'001';
		}
		return $pin;
	}

	public function importGuru($guru) {
		$success = [];
		$errors = [];
		$errorlist = '';
		foreach ($guru as $g) {
			if ($this->guruByNIPOnly($g->nip)) {
				# jika ada cek apakah guru masih aktif
				if ($g->teacher_isactive == '1') {
					# jika guru aktif, update guru
					$res = $this->updateGuruByNIP($g->nip, ['nama' => $g->teacher_name]);
				} else {
					# jika tidak aktif hapus guru
					$res = $this->hapusGuruByNIP($g->nip);
				}
			} else {
				# jika data guru belum ada di client/guru baru
				$pin = $this->createPIN();
				$res = $this->insertGuru(['nip' => $g->nip, 'pin' => $pin, 'nama' => $g->teacher_name]);
			}
			if ($res === true) {
				array_push($success, $g->nip.' '.$g->teacher_name);
			}
			if ($res === false) {
				array_push($errors, $g->nip.' '.$g->teacher_name);
			}
		}
		$this->session->set_flashdata('msg',count($success).' berhasil disinkronisasi. '.count($errors).' gagal sinkron.');
		foreach ($errors as $key) {
			$errorlist .= $key.'<br>';
		}
		$this->session->set_flashdata('errors',$errorlist);
		#return 'sukses';
	}

	public function jsonlog() {
		$sEcho = isset($_REQUEST['sEcho']) ? $_REQUEST['sEcho'] : 0;
        $aColumns = array( 'logfpguru.nip', 'guru.nama', 'logfpguru.waktu' );
        
        // paging
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
        }
        
        $sWhere = "";

        // searching
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = "Where (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sSearch = $this->db->escape_like_str( $_GET['sSearch'] );
                if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" )
                {
                    $sWhere .= $aColumns[$i]." LIKE '%".$sSearch."%' OR ";
                }
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        /* filtering */
        $var = "";
        for ($s = 0; $s < count($aColumns); $s++) {
            if (isset($_GET['sSearch_'.$s]) && $_GET['sSearch_'.$s] != "") {
                if ($var != "") {
                    $var .= " AND ";
                }
                $sSearch = $this->db->escape_like_str( $_GET['sSearch_'.$s] );
                if ( isset($_GET['bSearchable_'.$s]) && $_GET['bSearchable_'.$s] == "true" && $_GET['bSearchable_'.$s] != "" )
                {
                	if ($aColumns[$s] == 'logfpguru.waktu') {
                		$var .= $aColumns[$s]." LIKE '".$sSearch."%'";
                	} else {
                		$var .= $aColumns[$s]." LIKE '%".$sSearch."%'";
                	}
                }
            }
        }
        if ($var != "") {
        	if ($sWhere == "") {
        		$sWhere .= "Where (".$var.")";
        	} else {
        		$sWhere .= "And (".$var.")";
        	}
        }

        /* Ordering */
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                	$sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                        ".($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
             
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                $sOrder = "";
            }
        }
        
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS logfpguru.*, guru.nama as nama, fpmesin.mesin as mesin 
            FROM logfpguru inner join guru on logfpguru.nip = guru.nip 
            inner join fpmesin on logfpguru.mesin = fpmesin.idmesin 
            $sWhere
            $sOrder
            $sLimit
        ";
        
        $str = $this->db->query($sQuery);

        $sQuery = "
            SELECT FOUND_ROWS() As ftol
        ";
        
        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->row();
        $iFilteredTotal = $aResultFilterTotal->ftol;
    
        $strc = $this->db->query("SELECT COUNT(logfpguru.idlog) as idp FROM logfpguru inner join guru on logfpguru.nip = guru.nip inner join fpmesin on logfpguru.mesin = fpmesin.idmesin $sWhere");
        $res = $strc->row();
        $iTotal = $res->idp;
    
        $output = array(
            "sEcho" => intval($sEcho),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );
        
        $restr = $str->result();
        foreach($restr as $ary)
        {
        	switch ($ary->tipe) {
        		case 0:
        			$label = 'primary';
        			$caption = 'Absen masuk';
        			break;
        		case 1:
        			$label = 'warning';
        			$caption = 'Absen keluar';
        			break;
        		case 4:
        			$label = 'success';
        			$caption = 'Lembur masuk';
        			break;
        		case 5:
        			$label = 'danger';
        			$caption = 'Lembur keluar';
        			break;
        		
        		default:
        			$label = 'default';
        			$caption = 'Tanpa status';
        			break;
        	}
        	$tipe = "<span class='label label-$label'>$caption</span>";

        	$output['aaData'][] = array("0" => $ary->nip,
                                        "1" => $ary->nama,
                                        "2" => $ary->waktu,
                                    	"3" => $ary->mesin,
                                    	"4" => $tipe);
        }
        return json_encode($output);
	}

	public function json() {
		$sEcho = isset($_REQUEST['sEcho']) ? $_REQUEST['sEcho'] : 0;
        $aColumns = array( 'guru.nip', 'guru.pin', 'guru.nama' );
        
        // paging
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
        }
        
        $sWhere = "";

        // searching
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = "Where (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sSearch = $this->db->escape_like_str( $_GET['sSearch'] );
                if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" )
                {
                    $sWhere .= $aColumns[$i]." LIKE '%".$sSearch."%' OR ";
                }
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        /* filtering */
        $var = "";
        for ($s = 0; $s < count($aColumns); $s++) {
            if (isset($_GET['sSearch_'.$s]) && $_GET['sSearch_'.$s] != "") {
                if ($var != "") {
                    $var .= " AND ";
                }
                $sSearch = $this->db->escape_like_str( $_GET['sSearch_'.$s] );
                if ( isset($_GET['bSearchable_'.$s]) && $_GET['bSearchable_'.$s] == "true" && $_GET['bSearchable_'.$s] != "" )
                {
                    $var .= $aColumns[$s]." LIKE '%".$sSearch."%'";
                }
            }
        }
        if ($var != "") {
        	if ($sWhere == "") {
        		$sWhere .= "Where (".$var.")";
        	} else {
        		$sWhere .= "And (".$var.")";
        	}
        }

        /* Ordering */
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                        ".($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
             
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                $sOrder = "";
            }
        }
        
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS guru.*, count(jariguru.nip) as jariguruku 
            FROM guru left join jariguru on guru.nip = jariguru.nip
            $sWhere
            group by guru.nip
            $sOrder
            $sLimit
        ";
        
        $str = $this->db->query($sQuery);

        $sQuery = "
            SELECT FOUND_ROWS() As ftol
        ";
        
        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->row();
        $iFilteredTotal = $aResultFilterTotal->ftol;
    
        $strc = $this->db->query("SELECT COUNT(guru.nip) as idp FROM guru left join jariguru on guru.nip = jariguru.nip $sWhere group by guru.nip");
        $res = $strc->row();
        @$iTotal = $res->idp;
    
        $output = array(
            "sEcho" => intval($sEcho),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );
        
        $restr = $str->result();
        foreach($restr as $ary)
        {
        	if ($ary->jariguruku == 0) {
        		$jariguru = '<a href="#" class="btn btn-sm btn-rounded btn-warning" title="Belum ada sidik jariguru"><span class="fa fa-thumbs-down"></span> 0</a>';
        	} else {
        		$jariguru = '<a href="'.base_url('guru/delfp/'.$ary->nip).'" class="btn btn-sm btn-rounded btn-success" title="Sidik jariguru ok, klik untuk hapus"><span class="fa fa-thumbs-up"></span> '.$ary->jariguruku.'</a>';
        	}

        	if ($ary->pswd == null or $ary->pswd == '') {
        		$pswd = '<a href="#" class="btn btn-rounded btn-warning" title="Verifikasi sandi"><span class="fa fa-unlock-alt"></span> 0</a>';
        	} else {
        		$pswd = '<a href="#" class="btn btn-rounded btn-success" title="Verifikasi sandi"><span class="fa fa-lock"></span> 1</a>';
        	}

        	$ver = $jariguru." ".$pswd;
            $output['aaData'][] = array("0" => $ary->nip,
            							"1" => $ary->pin,
                                        "2" => $ary->nama,
                                        "3" => $ver);
        }
        return json_encode($output);
	}

}
