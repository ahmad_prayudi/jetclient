<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Siswamdl extends CI_Model {

	public function siswaById($id) {
		if ($this->clientmdl->getConfig('idsiswa') == 'nis') {
			return $this->db->where('nis',$id)->get('siswa')->row();
		} else {
			return $this->db->where('nosj',$id)->get('siswa')->row();
		}
	}

}
