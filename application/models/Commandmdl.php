<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Commandmdl extends CI_Model {

	public function getCommandRun($ip, $date) {
		$cmd = $this->db->where(['ip' => $ip, 'tanggal' => $date])->get('commands')->row();
		return $cmd;
	}

	public function countdelCmdRun($ip, $date) {
		$del = $this->db->where(['ip' => $ip, 'tanggal' => $date, 'cmdtype' => 'delete_log'])->get('commands')->num_rows();
		return $del;
	}

}
