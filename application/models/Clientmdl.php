<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Clientmdl extends CI_Model {

	public function parser($data,$p1,$p2){
		$data=" ".$data;
		$hasil="";
		$awal=strpos($data,$p1);
		if($awal!=""){
			$akhir=strpos(strstr($data,$p1),$p2);
			if($akhir!=""){
				$hasil=substr($data,$awal+strlen($p1),$akhir-strlen($p1));
			}
		}
		return $hasil;	
	}

	public function getPresenceType(int $number) {
		switch ($number) {
			case 0:
				return 'attend';
				break;
			case 1:
				return 'out';
				break;
			case 4:
				return 'ovtlogin';
				break;
			case 5:
				return 'ovtlogout';
				break;
			default:
				return null;
				break;
		}
	}

	public function getClientIP() {
		$ip = 'http://127.0.0.1';
		if(!empty($_SERVER['HTTP_CLIENT_IP'])) {
	        //ip from share internet
	        $ip = $_SERVER['HTTP_CLIENT_IP'];
	    }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	        //ip pass from proxy
	        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    }else {
	        $ip = $_SERVER['REMOTE_ADDR'];
	    }
	    return $ip;
	}

	public function getallsiswa() {
		$res = $this->db->get('siswa')->result();
		return $res;
	}

	public function createNOSJ() {
		$year = date('y');
		$this->db->select_max('nosj');
		$this->db->where('LEFT(nosj,2)',$year);
		$r = $this->db->get('siswa');
		if ($r->num_rows() > 0) {
			$no = $r->row()->nosj;
			$sno = substr($no,2,4);
			$ino = intval($sno) + 1;
			$num = str_pad($ino, 4, '0', STR_PAD_LEFT);
			$nosj = $year.$num;
		} else {
			$nosj = $year.'0001';
		}
		return $nosj;
	}

	public function enumValue($table, $field) {
		$type = $this->db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->row(0)->Type;
	    preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
	    $enum = explode("','", $matches[1]);
	    return $enum;
	}

	public function optionSelected($val,$key) {
		if ($val == $key) {
			return "selected";
		}
	}

	public function fpConnect($ip,$key,$port = '80') {
		$Connect = @fsockopen($ip, $port, $errno, $errstr, 1);
		if ($Connect) {
			return 1;
		} else {
			return 0;
		}
	}

	public function getLog($ip,$key,$port = '80') {
		$Connect = @fsockopen($ip, $port, $errno, $errstr, 1);
		if($Connect){
			$soap_request = "<GetAttLog><ArgComKey xsi:type=\"xsd:integer\">".$key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">All</PIN></Arg></GetAttLog>";
			$newLine = "\r\n";
			@fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
		    @fputs($Connect, "Content-Type: text/xml".$newLine);
		    @fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
		    @fputs($Connect, $soap_request.$newLine);
			$buffer = "";
			while($Response = fgets($Connect, 1024)){
				$buffer = $buffer.$Response;
			}

			$buffer = $this->parser($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");
			$buffer=explode("\r\n",$buffer);

			$out = array();
			for ($a = 0; $a < count($buffer); $a++) {
				$data = $this->parser($buffer[$a],"<Row>","</Row>");
				$pin = $this->parser($data,"<PIN>","</PIN>");
				$sts = $this->parser($data,"<Status>","</Status>");
            	if ($pin != '') {
					if ($sts == 0 or $sts == 1) {
                        $outs = array('pin' => $pin,
                                    'jam' => $this->parser($data,"<DateTime>","</DateTime>"),
                                    'ver' => $this->parser($data,"<Verified>","</Verified>"),
                                    'sts' => $sts);
                        array_push($out, $outs);
                    }
                }
			}
			return $out;
		} else {
			return "Gagal terhubung ke mesin dengan IP $ip";
		}
	}

	public function deleteLogAll() {
		$mesin = $this->getallfp();
		$autodel = $this->getConfig('autodel_log');
		# remove space
		$autodel = preg_replace('/\s+/','',$autodel);
		$port = '80';
		$outbuf = '';
		foreach ($mesin as $m) {
			$buff = $this->deleteLogMechine($m);
			$outbuf .= $m->ipmesin.' '.$buff.'\n';
		}
		return $outbuf;
	}

	public function deleteLogMechine($m = array()) {
		$autodel = $this->getConfig('autodel_log');
		# remove space
		$autodel = preg_replace('/\s+/','',$autodel);
		$buffer = 'bukan jadwal hapus log';
		if ($autodel != '0') {
			# pisahkan antara format jam dan durasi yg ditandai dengan simbol +
			$jamhapus = explode('+', $autodel);
			# tambahkan tanggal hari ini dengan jam hapus log
			$datedel = date('Y-m-d').' '.$jamhapus[0];
			# jika tanggal dan jam hari ini lebih besar dari pada jam hapus log
			if (date('Y-m-d H:i') > date('Y-m-d H:i', strtotime($datedel))) {
				# buat jam untuk durasi aktif hapus log
				# buang menit pada jamhapus[0]
				$expsubs = substr($jamhapus[0], 0, -2);
				# tambahkan menit baru dari jamhapus[1]
				$exptime = $expdata[0].$jamhapus[1];
				# gabungkan dengan tanggal hari ini
				$durasi = date('Y-m-d').' '.$exptime;
				# jika tanggal jam hari ini lebih besar atau sama dengan durasi
				if (date('Y-m-d H:i') >= date('Y-m-d H:i', strtotime($durasi))) {
					# maka beri informasi proses delete tidak aktif
					return "Auto delete sudah tidak aktif";
				}
				
				$Connect = @fsockopen($m->ipmesin, '80', $errno, $errstr, 1);
				if ($Connect) {
					$soap_request="<ClearData><ArgComKey xsi:type=\"xsd:integer\">".$m->comkey."</ArgComKey><Arg><Value xsi:type=\"xsd:integer\">3</Value></Arg></ClearData>";
					$newLine="\r\n";
					@fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
				    @fputs($Connect, "Content-Type: text/xml".$newLine);
				    @fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
				    @fputs($Connect, $soap_request.$newLine);
					$buffer="";
					while($Response=fgets($Connect, 1024)){
						$buffer=$buffer.$Response;
					}
					$buffer = $this->parser($buffer,"<Information>","</Information>");
				} else {
					$buffer = "Gagal koneksi";
				}
			}
		}
		return $buffer;
	}

	public function deleteUserMechine($ip = null, $com = null, $nis = null) {
		$Connect = @fsockopen($ip, '80', $errno, $errstr, 1);
		if ($Connect) {
			$soap_request="<DeleteUser><ArgComKey xsi:type=\"xsd:integer\">".$com."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">".$nis."</PIN></Arg></DeleteUser>";
			$newLine="\r\n";
			@fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
		    @fputs($Connect, "Content-Type: text/xml".$newLine);
		    @fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
		    @fputs($Connect, $soap_request.$newLine);
			$buffer="";
			while($Response=fgets($Connect, 1024)){
				$buffer=$buffer.$Response;
			}
			$buffer = $this->parser($buffer,"<DeleteUserResponse>","</DeleteUserResponse>");
			$buffer = $this->parser($buffer,"<Information>","</Information>");
		} else {
			$buffer = $ip." not connected...";
		}
		return $buffer;
	}

	public function siswaByNOSJ($nosj) {
		$this->db->where('nosj',$nosj);
		return $this->db->get('siswa')->row();
	}

	public function siswaByNIS($nis) {
		$this->db->where('nis',$nis);
		return $this->db->get('siswa')->row();
	}

	public function ambilSiswa($s) {
		if ($this->clientmdl->getConfig('idsiswa') == 'nis') {
    		$siswa = $this->clientmdl->siswaByNIS($s);
		} else {
    		$siswa = $this->clientmdl->siswaByNOSJ($s);
		}
		return $siswa;
	}

	public function getuserpasswordfp($ip, $key, $pin, $port = '80') {
		$Connect = @fsockopen($ip, '80', $errno, $errstr, 1);
		$soap_request="<GetUserInfo><ArgComKey xsi:type=\"xsd:integer\">".$key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">".$pin."</PIN></Arg></GetUserInfo>";
		$newLine="\r\n";
		@fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
	    @fputs($Connect, "Content-Type: text/xml".$newLine);
	    @fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
	    @fputs($Connect, $soap_request.$newLine);
		$buffer="";
		while($Response = @fgets($Connect, 1024)){
			$buffer=$buffer.$Response;
		}
		$buffer = $this->parser($buffer,"<GetUserInfoResponse>","</GetUserInfoResponse>");
		$buffer=explode("\r\n",$buffer);

		if ($pin != '') {
			if ($this->clientmdl->getConfig('idsiswa') == 'nis') {
				$siswa = $this->siswaByNIS($pin);
			} else {
				$siswa = $this->siswaByNOSJ($pin);
			}
			
			$nis = $siswa->nis;
			if (count($buffer) >= 3) {
				for($a = 0; $a < count($buffer); $a++) {
					$data = $this->parser($buffer[$a],"<Row>","</Row>");
					$pin = $this->parser($data,"<PIN>","</PIN>");
					$pswd = $this->parser($data,"<Password>","</Password>");
					if ($pin != '') {
						$this->db->where(array('nis' => $nis));
						$this->db->update('siswa', array('pswd' => $pswd));
						break;
					}
				}
			}
		}
	}

	public function getFPbyNIS($nis) {
		return $this->db->where('nis',$nis)->get('jari')->result();
	}

	public function getUserInfo($ip, $key, $pin, $port = '80') {
		mulai:
		$Connect = @fsockopen($ip, '80', $errno, $errstr, 1);
		if ($Connect) {
			$soap_request="<GetUserInfo><ArgComKey Xsi:type=\"xsd:integer\">".$Key."</ArgComKey><Arg><PIN>".$id."</PIN></GetUserInfo>";
			$newLine="\r\n";
        	fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
        	fputs($Connect, "Content-Type: text/xml".$newLine);
        	fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
        	fputs($Connect, $soap_request.$newLine);
        	$buffer="";
			while($Response=fgets($Connect, 1024)){
				$buffer=$buffer.$Response;
			}
			$buffer=$this->parser($buffer,"<GetUserInfoResponse>","</GetUserInfoResponse>");
			$buffer=$this->parser($buffer,"<Information>","</Information>");
			return $buffer;
		}
		return false;
	}

	public function refreshMechine($ip, $key, $port = '80') {
		mulai:
		# refresh
    	$Connect = @fsockopen($ip, '80', $errno, $errstr, 1);
    	if ($Connect) {
        	$soap_request="<RefreshDB><ArgComKey xsi:type=\"xsd:integer\">".$key."</ArgComKey></RefreshDB>";
        	$newLine="\r\n";
        	fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
        	fputs($Connect, "Content-Type: text/xml".$newLine);
        	fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
        	fputs($Connect, $soap_request.$newLine);
        	fclose($Connect);
        	return true;
    	} else {
    		goto mulai;
    	}
    	return false;
	}
	
	public function setusertemplatefp($ip, $key, $pin, $port = '80') {
		if ($this->clientmdl->getConfig('idsiswa') == 'nis') {
        	$siswa = $this->siswaByNIS($pin);
		} else {
			$siswa = $this->siswaByNOSJ($pin);
		}
		if ($siswa) {
        	$nis = $siswa->nis;
        } else {
        	return false;
        }
			
		# get finger database
		$fp = $this->getFPbyNIS($nis);
		if (count($fp) > 0) {
			foreach ($fp as $f) {
				mulai:
				$Connect = @fsockopen($ip, '80', $errno, $errstr, 1);
				if ($Connect) {
					$fn = $f->nomor;
					$temp = $f->template;
					$soap_request="<SetUserTemplate><ArgComKey xsi:type=\"xsd:integer\">".$key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">".$pin."</PIN><FingerID xsi:type=\"xsd:integer\">".$fn."</FingerID><Size>".strlen($temp)."</Size><Valid>1</Valid><Template>".$temp."</Template></Arg></SetUserTemplate>";
					$newLine="\r\n";
					@fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
					@fputs($Connect, "Content-Type: text/xml".$newLine);
					@fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
					@fputs($Connect, $soap_request.$newLine);
					$buffer="";
					while($Response=fgets($Connect, 1024)){
						$buffer=$buffer.$Response;
					}
					$buffer=$this->parser($buffer,"<SetUserTemplateResponse>","</SetUserTemplateResponse>");
					$buffer=$this->parser($buffer,"<Information>","</Information>");
					$this->refreshMechine($ip, $key);
					echo $pin." ".$fn." sukses<br>";
				} else {
					goto mulai;
				}
			}
			#return true;
			echo $pin." ".$fn." sukses<br>";
		} else {
			#return false;
			echo $pin." tidak ditemukan rekaman sidik jari<br>";
		}
    }

	public function getusertemplatefp($ip, $key, $pin, $port = '80') {
		$Connect = @fsockopen($ip, '80', $errno, $errstr, 1);
		$soap_request = "<GetUserTemplate><ArgComKey xsi:type=\"xsd:integer\">".$key."</ArgComKey><Arg><PIN xsi:type=\"xsd:integer\">".$pin."</PIN><FingerID xsi:type=\"xsd:integer\"></FingerID></Arg></GetUserTemplate>";
		$newLine="\r\n";
		@fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
	    @fputs($Connect, "Content-Type: text/xml".$newLine);
	    @fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
	    @fputs($Connect, $soap_request.$newLine);
		$buffer="";
		while($Response = @fgets($Connect, 1024)){
			$buffer=$buffer.$Response;
		}
		$buffer = $this->parser($buffer,"<GetUserTemplateResponse>","</GetUserTemplateResponse>");
		$buffer=explode("\r\n",$buffer);

		# ambil data siswanya
		if ($pin != '') {
			if ($this->clientmdl->getConfig('idsiswa') == 'nis') {
				$siswa = $this->siswaByNIS($pin);
			} else {
				$siswa = $this->siswaByNOSJ($pin);
			}
			$nis = $siswa->nis;
			if (count($buffer) >= 3) {
				for($a = 0; $a < count($buffer); $a++) {
					$data = $this->parser($buffer[$a],"<Row>","</Row>");
					$pin = $this->parser($data,"<PIN>","</PIN>");
					if ($pin != '') {
						$fid = $this->parser($data,"<FingerID>","</FingerID>");
						$siz = $this->parser($data,"<Size>","</Size>");
						$tpl = $this->parser($data,"<Template>","</Template>");
						$jari = $this->db->get_where('jari', array('nis' => $nis, 'nomor' => $fid));
						if ($jari->num_rows() == 0) {
							$data = array('nis' => $nis, 'nomor' => $fid, 'ukuran' => $siz, 'template' => $tpl);
							$this->db->insert('jari', $data);
						} else {
							$data = array('ukuran' => $siz, 'template' => $tpl);
							$wh = array('nis' => $nis, 'nomor' => $fid);
							$this->db->where($wh);
							$this->db->update('jari', $data);             
						}
					}
				}
			}
		}
	}

	public function tablefplog() {
		//$this->output->enable_profiler(TRUE);
		$sEcho = isset($_REQUEST['sEcho']) ? $_REQUEST['sEcho'] : 0;
		$fp = $this->getAllfp();
		$rw = array();
		$tr = '<tbody>';
		foreach ($fp as $row) {
			$st = null;
			$st = $this->fpConnect($row->ipmesin,$row->comkey);
			if ($st == 1) {
				$soap = $this->getlog($row->ipmesin,$row->comkey);
				foreach ($soap as $val) {
					if ($val['ver'] == 1) {
						$ver = 'Jari';
					} elseif ($val['ver'] == 3) {
						$ver = 'Password';
					}

					if ($val['sts'] == 0) {
						$sts = 'Masuk';
					} elseif ($val['sts'] == 1) {
						$sts = 'Pulang';
					}

					$tr .= '<tr>';
					$tr .= '<td>'.$val['nis'].'</td>';
					$tr .= '<td>'.$val['jam'].'</td>';
					$tr .= '<td>'.$ver.'</td>';
					$tr .= '<td>'.$sts.'</td>';
					$tr .= '</tr>';
				}
			}
		}
		$tr .= '</tbody>';
		return $tr;
	}

	public function getConfig($key) {
		$row = $this->db->get_where('jetconfig', array('jetkey' => $key))->row();
		return $row->jetval;
	}

	public function tableAdmin() {
		$res = $this->db->get('admin')->result();
		$tr = "";
		$tr = "<tbody>";
		foreach ($res as $val) {
			$btn = "";
			if ($this->session->userdata('akses') == 'Admin') {
				$ulink = site_url('admin/adminform/edit/'.$val->idadmin);
				$dlink = site_url('client/admin/delete/'.$val->idadmin);
				$btn .= '<a href="'.$ulink.'" class="btn btn-sm btn-warning" title="Edit data">';
				$btn .= '<span class="fa fa-cog"></span> Edit</a>&nbsp;';
				$btn .= '<a href="'.$dlink.'" class="btn btn-sm btn-danger" title="Hapus data">';
				$btn .= '<span class="fa fa-trash"></span> Hapus</a>';
			} else {
				if ($this->session->userdata('id') == $val->admincode) {
					$ulink = site_url('admin/adminform/edit/'.$val->idadmin);
					$btn .= '<a href="'.$ulink.'" class="btn btn-sm btn-default" title="Edit data">';
					$btn .= '<span class="fa fa-cog"></span> Edit</a>';
				}
			}
			
			$tr .= '<tr>';
			$tr .= '<td>'.$val->admincode.'</td>';
			$tr .= '<td>'.$val->adminname.'</td>';
			$tr .= '<td>'.$val->akses.'</td>';
			$tr .= '<td>'.$val->lastlog.'</td>';
			$tr .= '<td>'.$btn.'</td>';
			$tr .= '</tr>';
		}
		$tr .= "</tbody>";
		return $tr;
	}

	public function getAllfp() {
		$res = $this->db->get('fpmesin')->result();
		return $res;
	}

	public function getAdminrow($id) {
		$row = $this->db->get_where('admin', array('idadmin' => $id))->row();
		return $row;
	}

	public function sendLog($data) {
		$p = $this->clientmdl->getConfig('url_api');
		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, $p.'/syncPresence'); 
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_POST, 1);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$tmp = curl_exec ($ch);
		curl_close ($ch);
		$tempe = json_decode($tmp);
		if (count($tempe) > 0) {
			foreach ($tempe as $value) {
				$sync = $value->sync;
				$sub = $value->subject;
				if ($sync == '1') {
					if ($sub == 'student') {
						$this->db->where(['nis' => $value->nis, 'waktu' => $value->waktu]);
						if (!$this->db->update('logfp', ['sync' => '1'])) {
							echo "Gagal sinkron ".$value->nis." ".$value->tipe." ".$value->type." ".$value->waktu." ".$sync."<br/>";
						}
					} else {
						$this->db->where(['nip' => $value->nip, 'waktu' => $value->waktu]);
						if (!$this->db->update('logfpguru', ['sync' => '1'])) {
							echo "Gagal sinkron ".$value->nip." ".$value->tipe." ".$value->type." ".$value->waktu." ".$sync."<br/>";
						}
					}
				}
			}
			return $tempe;
		} else {
			return "gagal";
		}
	}

	public function jsonsiswa() {
		$sEcho = isset($_REQUEST['sEcho']) ? $_REQUEST['sEcho'] : 0;
        $aColumns = array( 'siswa.nis', 'siswa.nosj', 'siswa.nama', 'jariku' );
        
        // paging
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
        }
        
        $sWhere = "";

        // searching
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = "Where (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sSearch = $this->db->escape_like_str( $_GET['sSearch'] );
                if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" )
                {
                    $sWhere .= $aColumns[$i]." LIKE '%".$sSearch."%' OR ";
                }
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        /* filtering */
        $var = "";
        for ($s = 0; $s < count($aColumns); $s++) {
            if (isset($_GET['sSearch_'.$s]) && $_GET['sSearch_'.$s] != "") {
                if ($var != "") {
                    $var .= " AND ";
                }
                $sSearch = $this->db->escape_like_str( $_GET['sSearch_'.$s] );
                if ( isset($_GET['bSearchable_'.$s]) && $_GET['bSearchable_'.$s] == "true" && $_GET['bSearchable_'.$s] != "" )
                {
                    $var .= $aColumns[$s]." LIKE '%".$sSearch."%'";
                }
            }
        }
        if ($var != "") {
        	if ($sWhere == "") {
        		$sWhere .= "Where (".$var.")";
        	} else {
        		$sWhere .= "And (".$var.")";
        	}
        }

        /* Ordering */
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                        ".($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
             
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                $sOrder = "";
            }
        }
        
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS siswa.*, count(jari.nis) as jariku 
            FROM siswa left join jari on siswa.nis = jari.nis
            $sWhere
            group by siswa.nis
            $sOrder
            $sLimit
        ";
        
        $str = $this->db->query($sQuery);

        $sQuery = "
            SELECT FOUND_ROWS() As ftol
        ";
        
        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->row();
        $iFilteredTotal = $aResultFilterTotal->ftol;
    
        $strc = $this->db->query("SELECT COUNT(siswa.nis) as idp FROM siswa left join jari on siswa.nis = jari.nis $sWhere group by siswa.nis");
        $res = $strc->row();
        @$iTotal = $res->idp;
    
        $output = array(
            "sEcho" => intval($sEcho),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );
        
        $restr = $str->result();
        foreach($restr as $ary)
        {
        	if ($ary->jariku == 0) {
        		$jari = '<a href="#" class="btn btn-sm btn-rounded btn-warning" title="Belum ada sidik jari"><span class="fa fa-thumbs-down"></span> 0</a>';
        		$j = '<div class="btn btn-sm btn-rounded btn-warning">0</div>';
        	} else {
        		$jari = '<a href="'.base_url('siswa/delfp/'.$ary->nis).'" class="btn btn-sm btn-rounded btn-success" title="Sidik jari ok, klik untuk hapus"><span class="fa fa-thumbs-up"></span> '.$ary->jariku.'</a>';
        		$j = '<div class="btn btn-sm btn-rounded btn-success">'.$ary->jariku.'</div>';
        	}

        	if ($ary->pswd == null or $ary->pswd == '') {
        		$pswd = '<a href="#" class="btn btn-rounded btn-warning" title="Verifikasi sandi"><span class="fa fa-unlock-alt"></span> 0</a>';
        	} else {
        		$pswd = '<a href="#" class="btn btn-rounded btn-success" title="Verifikasi sandi"><span class="fa fa-lock"></span> 1</a>';
        	}

        	$ver = $jari." ".$pswd;
            $output['aaData'][] = array("0" => $ary->nis,
            							"1" => $ary->nosj,
                                        "2" => $ary->nama,
                                        "3" => $j);
        }
        return json_encode($output);
	}

	public function jsonpresence() {
		$sEcho = isset($_REQUEST['sEcho']) ? $_REQUEST['sEcho'] : 0;
        $aColumns = array( 'logfp.nis', 'siswa.nama', 'logfp.waktu' );
        
        // paging
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
        {
            $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".intval( $_GET['iDisplayLength'] );
        }
        
        $sWhere = "";

        // searching
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" )
        {
            $sWhere = "Where (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $sSearch = $this->db->escape_like_str( $_GET['sSearch'] );
                if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" )
                {
                    $sWhere .= $aColumns[$i]." LIKE '%".$sSearch."%' OR ";
                }
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }

        /* filtering */
        $var = "";
        for ($s = 0; $s < count($aColumns); $s++) {
            if (isset($_GET['sSearch_'.$s]) && $_GET['sSearch_'.$s] != "") {
                if ($var != "") {
                    $var .= " AND ";
                }
                $sSearch = $this->db->escape_like_str( $_GET['sSearch_'.$s] );
                if ( isset($_GET['bSearchable_'.$s]) && $_GET['bSearchable_'.$s] == "true" && $_GET['bSearchable_'.$s] != "" )
                {
                	if ($aColumns[$s] == 'logfp.waktu') {
                		$var .= $aColumns[$s]." LIKE '".$sSearch."%'";
                	} else {
                		$var .= $aColumns[$s]." LIKE '%".$sSearch."%'";
                	}
                }
            }
        }
        if ($var != "") {
        	if ($sWhere == "") {
        		$sWhere .= "Where (".$var.")";
        	} else {
        		$sWhere .= "And (".$var.")";
        	}
        }

        /* Ordering */
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) )
        {
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
            {
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
                {
                	$sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
                        ".($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
             
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" )
            {
                $sOrder = "";
            }
        }
        
        $sQuery = "
            SELECT SQL_CALC_FOUND_ROWS logfp.*, siswa.nama as nama, fpmesin.mesin as mesin 
            FROM logfp inner join siswa on logfp.nis = siswa.nis 
            inner join fpmesin on logfp.mesin = fpmesin.idmesin 
            $sWhere
            $sOrder
            $sLimit
        ";
        
        $str = $this->db->query($sQuery);

        $sQuery = "
            SELECT FOUND_ROWS() As ftol
        ";
        
        $rResultFilterTotal = $this->db->query($sQuery);
        $aResultFilterTotal = $rResultFilterTotal->row();
        $iFilteredTotal = $aResultFilterTotal->ftol;
    
        $strc = $this->db->query("SELECT COUNT(logfp.idlog) as idp FROM logfp inner join siswa on logfp.nis = siswa.nis inner join fpmesin on logfp.mesin = fpmesin.idmesin $sWhere");
        $res = $strc->row();
        $iTotal = $res->idp;
    
        $output = array(
            "sEcho" => intval($sEcho),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array()
        );
        
        $restr = $str->result();
        foreach($restr as $ary)
        {
        	if ($ary->tipe == 0) {
        		$tipe = '<span class="label label-primary">Masuk</span>';
        	} else {
        		$tipe = '<span class="label label-primary">Pulang</span>';
        	}
        	$output['aaData'][] = array("0" => $ary->nis,
                                        "1" => $ary->nama,
                                        "2" => $ary->waktu,
                                    	"3" => $ary->mesin,
                                    	"4" => $tipe);
        }
        return json_encode($output);
	}

}
