<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Authmdl extends CI_Model {

	public function isLogin() {
		if (empty($this->session->userdata('id'))) {
			redirect('page/login');
		} else {
			if (!$this->isAdmin($this->session->userdata('id'))) {
				$this->session->sess_destroy();
				redirect('page/login');
			}
		}
	}

	public function isAdmin($id) {
		$num = $this->clientmdl->getConfig('username');
		if ($num == $id) {
			return true;
		} else {
			return false;
		}
	}

	public function getAdminrow() {
		return $this->db->get_where('admin', array('admincode' => $this->session->userdata('id')))->row();
	}

	public function getAdminId() {
		$row = $this->db->get_where('admin', array('admincode' => $this->session->userdata('id')))->row();
		$id = $row->idadmin;
		return $id;
	}

	public function onlyAdmin() {
		if (strtolower($this->session->userdata('akses')) != 'admin') {
			redirect('page/notallowed');
		}
	}

	public function updateLog($id) {
		$where = array('idadmin' => $id);
		$data = array('lastlog' => date('Y-m-d H:i:s'));
		$this->db->where($where);
		$this->db->update('admin',$data);
	}

	public function signin_admin($data = array()) {
		$data['usercode'] = sha1($data['usercode']);
		$n = $this->clientmdl->getConfig('username');
		$c = $this->clientmdl->getConfig('usercode');
		if ($n == $data['username']) {
			if ($c == $data['usercode']) {
				$sessid = array("id" => $data['username'],
							"nama" => 'JetAdmin',
							"akses" => 'Admin');
				$this->session->set_userdata($sessid);
				redirect('welcome');
				//print_r($this->session->userdata());
			} else {
				$this->session->set_flashdata("errlog", "Password yang dimasukan tidak benar");
				redirect('page/login');
			}
		} else {
			$this->session->set_flashdata("errlog", "Username yang dimasukan tidak benar");
			redirect('page/login');
		}
		//print_r($data);
	}

}