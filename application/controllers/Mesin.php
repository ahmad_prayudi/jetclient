<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Mesin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function datatable() {
		$this->authmdl->onlyAdmin();
		$data['tabel'] = $this->mesinmdl->tableMesin();
		$this->slice->view('mesin.table',$data);
	}

	public function addfpm() {
		$this->authmdl->onlyAdmin();
		$data['idmesin'] = "";
		$data['mesin'] = "";
		$data['ipmesin'] = "";
		$data['comkey'] = "0";
		$data['mode'] = "insert";
		$this->slice->view('mesin.form',$data);
	}

	public function editfpm($id = null) {
		$this->authmdl->onlyAdmin();
		$row = $this->mesinmdl->getMesinrow($id);
		$data['idmesin'] = $id;
		$data['mesin'] = $row->mesin;
		$data['ipmesin'] = $row->ipmesin;
		$data['comkey'] = $row->comkey;
		$data['mode'] = "update";
		$this->slice->view('mesin.form',$data);
	}

}
