<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function updateconf() {
		$data = $this->input->post();
		foreach ($data as $key => $value) {
			if ($key == 'usercode') {
				if ($value != '') {
					$value = sha1($value);
					$ucode = $this->clientmdl->getConfig('usercode');
					if ($value == $ucode) {
						continue;
					}
				} else {
					continue;
				}
			}
			$this->db->where(array('jetkey' => $key));
			if (!$this->db->update('jetconfig', array('jetval' => $value))) {
				$this->session->set_flashdata('msg','Gagal melakukan update data '.$key);
				redirect('page/aplikasi');
			}
		}
		$this->session->set_flashdata('msg','Data konfigurasi berhasil diperbarui');
		redirect('page/aplikasi');
	}

	public function admin($mode, $id = null) {
		$data = $this->input->post();
		if ($mode == 'insert') {
			$data['adminpswd'] = sha1($data['adminpswd']);
			$this->db->insert('admin', $data);
		} elseif ($mode == 'update') {
			if ($data['adminpswd'] == '') {
				unset($data['adminpswd']);
			} else {
				$data['adminpswd'] = sha1($data['adminpswd']);
			}
			
			$where = array('idadmin' => $id);
			$this->db->where($where);
			$this->db->update('admin', $data);
		} elseif ($mode == 'delete') {
			$where = array('idadmin' => $id);
			$this->db->where($where);
			$this->db->delete('admin');
		}

		if ($this->db->affected_rows() == 0) {
			$this->session->set_flashdata('msg', 'Gagal melakukan '.$mode.' data');
			$this->session->set_flashdata('num', 0);
		} else {
			$this->session->set_flashdata('msg', $mode. ' data berhasil');
			$this->session->set_flashdata('num', 1);
		}
		redirect('admin/datatable');
	}

	public function mesin($mode, $id = null) {
		$data = $this->input->post();
		if ($mode == 'insert') {
			$data['added'] = date('Y-m-d H:i:s');
			$this->db->insert('fpmesin', $data);
		} elseif ($mode == 'update') {
			$where = array('idmesin' => $id);
			$this->db->where($where);
			$this->db->update('fpmesin', $data);
		} elseif ($mode == 'delete') {
			$where = array('idmesin' => $id);
			$this->db->where($where);
			$this->db->delete('fpmesin');
		}

		if ($this->db->affected_rows() == 0) {
			$this->session->set_flashdata('msg', 'Gagal melakukan '.$mode.' data');
			$this->session->set_flashdata('num', 0);
		} else {
			$this->session->set_flashdata('msg', $mode. ' data berhasil');
			$this->session->set_flashdata('num', 1);
		}
		redirect('mesin/datatable');
	}

	public function getfpstatus() {
		$fp = $this->clientmdl->getAllfp();
		$rw = array();
		foreach ($fp as $row) {
			$st = null;
			$st = $this->clientmdl->fpConnect($row->ipmesin,$row->comkey);
			if ($st == 0) {
				$sts = '<div id="sts'.$row->idmesin.'"><span class="badge badge-danger"></span> Offline</div>';
			} else {
				$sts = '<div id="sts'.$row->idmesin.'"><span class="badge badge-success"></span> Online</div>';
			}
			$rws = array('id' => $row->idmesin, 'sts' => $sts);
			array_push($rw, $rws);
		}
		echo json_encode($rw);
	}

	public function deletelogmechine($idm = null) {
		$m = $this->mesinmdl->getMesinrow($idm);
		if (!$this->clientmdl->deleteLogMechine($m)) {
			redirect('page/logmechine/'.$idm);
		} else {
			redirect('page/app_error');
		}
	}

	public function delLogMechines() {
		$mesin = $this->clientmdl->getallfp();
		foreach ($mesin as $m) {
			$this->clientmdl->deleteLogMechine($m);
		}
	}

}
