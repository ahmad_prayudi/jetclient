<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';

class Iclock extends Restserver\Libraries\REST_Controller {

    public function cdata_get() {
        $sn = $_GET['SN'];
        $ip = $this->clientmdl->getClientIP();
        $isi = ['mesin_sn' => $get['SN'], 'client_ip' => $ip, 'reqtype' => 'GET_cdata', 'uploaded' => json_encode($this->input->get())];
        $date = strtotime(date('Y-m-d 07:00:00'));
        //print_r($mesin->row());
        $echo = "GET OPTION FROM: ".$sn;
        $echo .= "\r\nStamp=".$date;
        $echo .= "\nOpStamp=".$date;
        $echo .= "\nErrorDelay=60";
        $echo .= "\nDelay=30";
        $echo .= "\nTransTimes=08:00;15:00";
        $echo .= "\nTransInterval=1";
        $echo .= "\nTransFlag=1111000000";
        $echo .= "\nRealtime=1";
        $echo .= "\nEncrypt=0";
        #$echo = ["GET OPTION FROM" => $sn, "Stamp" => $date, "OpStamp" => $date];
        echo $echo;
    }

    public function getrequest_get() {
        #echo "C:1:CLEAR LOG";
        # get request pada versi ini baru mendukung auto delete
        $autodel = $this->clientmdl->getConfig('autodel_log');
        if ($autodel != '0') {
        	$date = date('Y-m-d');
        	$hour = date('H:i');
        	# cek command hari ini untuk ip klien
        	$ip = $this->clientmdl->getClientIP();
        	$del = $this->commandmdl->countdelCmdRun($ip, $date);
        	if ($del == 0) {
        		$command = "C:1:CLEAR LOG";
        		if (strtotime($hour) >= strtotime($autodel)) {
        			$adel = ['ip' => $ip, 'tanggal' => $date, 'command' => $command, 'cmdtype' => 'delete_log'];
	        		$this->db->insert('commands', $adel);
	        		echo "C:1:CLEAR LOG";
        		}
        	}
        }
    }

    public function devicecmd_post() {
    	$sn = $_GET['SN'];
    	$dt = json_encode($this->input->post());
    	$by = json_encode(file_get_contents( 'php://input' ));
    	$isi = ['mesin_sn' => $sn, 'uploaded' => $dt.','.$by];
    	$this->db->insert('uploaded',$isi);
    	echo "OK";
    }

    public function cdata_post() {
    	$get = $this->input->get();
    	$ip = $this->clientmdl->getClientIP();
    	$mesin = $this->mesinmdl->getMachinebyIP($ip);
    	$input = file_get_contents( 'php://input' );
    	$isi = ['mesin_sn' => $get['SN'], 'client_ip' => $ip, 'reqtype' => 'POST_cdata', 'parameter' => json_encode($get), 'uploaded' => json_encode($input)];
    	$this->db->insert('uploaded',$isi);
    	# cek input jika parameter OPERLOG
    	if ($get['table'] == 'OPERLOG') {
    		$o = explode(" ", $input);
    		if ($o[0] == 'FP') {
    			# simpan finger print
    			echo 'OK';
    			return 'OK';
    		} elseif ($o[0] == 'OPLOG') {
    			# simpan finger print
    			echo 'OK';
    			return 'OK';
    		}
    		#echo 'OK';
    	}
    	# eksekusi kehadiran
    	$rows = explode("\n", $input);

    	$data = array('id_school' => $this->clientmdl->getConfig('sch_code'),
						'key_school' => $this->clientmdl->getConfig('sch_keys'),
						'presence_res' => 'finger',
						'presence' => array());
    	foreach ($rows as $r) {
    		if ($r != '') {
    			$cols = explode("\t", $r);
    			$tipe = $this->clientmdl->getPresenceType($cols[2]);
    			$org = null;
    			$siswa = true;
    			if ($this->siswamdl->siswaById($cols[0])) {
    				$org = $this->siswamdl->siswaById($cols[0]);
    			} else {
    				$org = $this->teachermdl->guruById($cols[0]);
    				$siswa = false;
    			}

    			$waktu = strtotime($cols[1]) - 3600;
    			$wkt = date('Y-m-d H:i:s', $waktu);
    			if ($org != null) {
    				$dout = ['id' => null, 'presence_sub' => null, 'presence_type' => $tipe, 'presence_log' => $wkt];
    				if ($siswa == false) {
						$p =['nip' => $org->nip, 'pin' => $org->pin, 'tipe' => $cols[2], 'waktu' => $wkt, 'sync' => '0', 'mesin' => '1'];
			    		$exe = $this->db->insert('logfpguru', $p);
			    		$dout['id'] = $org->nip;
			    		$dout['presence_sub'] = 'teacher';
					} else {
						$p =['nis' => $org->nis, 'pin' => $org->nosj, 'tipe' => $cols[2], 'waktu' => $wkt, 'sync' => '0', 'mesin' => '1'];
			    		$exe = $this->db->insert('logfp', $p);
			    		$dout['id'] = $org->nis;
			    		$dout['presence_sub'] = 'student';
					}
					array_push($data['presence'],$dout);
    			}
    		}
    	}
    	$data['presence'] = json_encode($data['presence']);
    	$send = $this->clientmdl->sendLog($data);
    	//print_r($send);
        echo "OK";
        #return 'OK';
        //print_r($data);
    }

}
