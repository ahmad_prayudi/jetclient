<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function datatable() {
		$this->authmdl->onlyAdmin();
		$this->slice->view('siswa.table');
	}

	public function jsondata() {
		$this->authmdl->onlyAdmin();
		echo $this->clientmdl->jsonsiswa();
	}

	public function kelola() {
		$this->authmdl->onlyAdmin();
		$data['mesin'] = $this->clientmdl->getallfp();
		$this->slice->view('siswa.kelola', $data);
	}

	public function delfp($nis) {
		$this->authmdl->onlyAdmin();
		$data['siswa'] = $this->clientmdl->siswaByNIS($nis);
		$data['mesin'] = $this->db->get('fpmesin')->result();
		$this->slice->view('siswa.delfp',$data);
	}

	public function sinkron() {
		$p = $this->clientmdl->getConfig('url_api');
		$data['id_school'] = $this->clientmdl->getConfig('sch_code');
		$data['key_school'] = $this->clientmdl->getConfig('sch_keys');
		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, $p.'/getStudent'); 
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_POST, 1);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$tmp = curl_exec ($ch);
		curl_close ($ch);
		$recent = $this->clientmdl->getallsiswa();
		if ($tmp) {
			$tempe = json_decode($tmp);
			foreach ($tempe as $key => $value) {
				if ($value->nis != null) {
					$num = $this->db->get_where('siswa', array('nis' => $value->nis))->num_rows();
					if ($num == 0) {
						$nosj = $this->clientmdl->createNOSJ();
						$data = array('nis' => $value->nis,
										'nosj' => $nosj,
										'nama' => $value->student_name);
						$this->db->insert('siswa',$data);
					} else {
						$data = array('nama' => $value->student_name);
						$where = array('nis' => $value->nis);
						$this->db->where($where);
						$this->db->update('siswa',$data);
					}
				}
			}
			//delete unnecessary student (student with active is 0) or is alumni
			foreach ($recent as $value) {
				if (!in_array($value->nis, array_column($tempe, 'nis'))) {
					$this->db->where(['nis' => $value->nis])->delete('siswa');
				}
			}
			redirect('siswa/datatable');
		} else {
			echo $tmp;
		}
	}

	public function ritual() {
		$data = $this->input->post();
		$row = $this->mesinmdl->getMesinrow($data['mesin']);
		$con = $this->clientmdl->fpConnect($row->ipmesin, $row->comkey);
		if ($con == 1) {
			$siswa = $this->clientmdl->getallsiswa();
			if ($data['mode'] == 0) {
				# download dari mesin
				$ap = array();
				$at = array();
				foreach ($siswa as $val) {
					if ($this->clientmdl->getConfig('idsiswa') == 'nis') {
						$pin = $val->nis;
					} else {
						$pin = $val->nosj;
					}
					$this->clientmdl->getuserpasswordfp($row->ipmesin, $row->comkey, $pin);
					$this->clientmdl->getusertemplatefp($row->ipmesin, $row->comkey, $pin);
				}
			} elseif ($data['mode'] == 1) {
				# upload ke mesin
				foreach ($siswa as $val) {
					konekid:
					$Connect = @fsockopen($row->ipmesin, '80', $errno, $errstr, 1);
					if ($Connect) {
						$nama = $val->nama;
						if ($this->clientmdl->getConfig('idsiswa') == 'nis') {
							$pin = $val->nis;
						} else {
							$pin = $val->nosj;
						}

						if ($val->pswd == null or $val->pswd == '') {
							$pswd = '';
						} else {
							$pswd = '<Password>'.$val->pswd.'</Password>';
						}
						$soap_request="<SetUserInfo><ArgComKey Xsi:type=\"xsd:integer\">".$row->comkey."</ArgComKey><Arg><PIN>".$pin."</PIN><Name>".$nama."</Name>".$pswd."</Arg></SetUserInfo>";
						$newLine="\r\n";
						@fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
						@fputs($Connect, "Content-Type: text/xml".$newLine);
						@fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
						@fputs($Connect, $soap_request.$newLine);
	                	fclose($Connect);
	                	$this->clientmdl->refreshMechine($row->ipmesin, $row->comkey);
	                	if (count($this->clientmdl->getFPbyNIS($val->nis)) > 0) {
	                		$this->clientmdl->setusertemplatefp($row->ipmesin, $row->comkey, $pin);
	                    }
					} else {
						goto konekid;
					}
				}
				$this->clientmdl->refreshMechine($row->ipmesin, $row->comkey);
			} elseif ($data['mode'] == 2) {
				foreach ($siswa as $val) {
					if ($this->clientmdl->getConfig('idsiswa') == 'nis') {
						$pin = $val->nis;
					} else {
						$pin = $val->nosj;
					}
					$this->clientmdl->deleteUserMechine($row->ipmesin, $row->comkey, $pin);
				}
			}
			//Refresh DB
			$Connect = @fsockopen($row->ipmesin, '80', $errno, $errstr, 1);
			$soap_request="<RefreshDB><ArgComKey xsi:type=\"xsd:integer\">".$row->comkey."</ArgComKey></RefreshDB>";
			$newLine="\r\n";
			@fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
			@fputs($Connect, "Content-Type: text/xml".$newLine);
			@fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
			@fputs($Connect, $soap_request.$newLine);

			$this->session->set_flashdata('num', 1);
			$this->session->set_flashdata('msg', 'Data selesai diperbarui');
			redirect('siswa/datatable');
		} else {
			echo "koneksi gagal";
		}
	}

}
