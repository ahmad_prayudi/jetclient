<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function signin()
	{
		$data = $this->input->post();
		$this->authmdl->signin_admin($data);
	}

	public function signout() {
		$this->session->sess_destroy();
		redirect('page/login');
	}

	public function sql() {
		# ALTER TABLE `siswa` ADD `nosj` INT(8) UNSIGNED NULL DEFAULT NULL AFTER `nis`;
	}
}
