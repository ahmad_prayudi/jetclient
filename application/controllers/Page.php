<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function login()
	{
		$this->slice->view('login');
	}

	public function notallowed() {
		$this->slice->view('page.noallow');
	}

	public function app_error() {
		$this->slice->view('page.app_error');
	}

	public function aplikasi() {
		$this->authmdl->onlyAdmin();
		$data['url_api'] = $this->clientmdl->getConfig('url_api');
		$data['sch_code'] = $this->clientmdl->getConfig('sch_code');
		$data['sch_keys'] = $this->clientmdl->getConfig('sch_keys');
		$data['username'] = $this->clientmdl->getConfig('username');
		$data['autodel_log'] = $this->clientmdl->getConfig('autodel_log');
		$data['usercode'] = '';
		$u = $this->clientmdl->getConfig('usercode');
		if ($u != "") {
			$data['u'] = "Password Ok";
		} else {
			$data['u'] = "Password belum diset";
		}
		$data['idsiswa'] = $this->clientmdl->getConfig('idsiswa');
		$data['idguru'] = $this->clientmdl->getConfig('idguru');
		$this->slice->view('page.faplikasi',$data);
	}

	public function guruatt() {
		$this->authmdl->onlyAdmin();
		$this->slice->view('guru.logpresence');
	}

	public function logpresence() {
		$this->authmdl->onlyAdmin();
		$this->slice->view('page.logpre');
	}

	public function logmechine($idm = null) {
		$this->authmdl->onlyAdmin();
		if ($idm == null or $idm == '' or $idm == 0) {
			$log = array();
		} else {
			$m = $this->mesinmdl->getMesinrow($idm);
			$log = $this->clientmdl->getLog($m->ipmesin,$m->comkey);
		}
		$data['log'] = $log;
		$data['idm'] = $idm;
		$data['mesin'] = $this->clientmdl->getAllfp();
		$this->slice->view('page.logmec',$data);
	}

}
