<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cli extends CI_Controller {

    public function insertPresenceLogs() {
		$mesin = $this->db->get('fpmesin')->result();
		foreach ($mesin as $key) {
			$c = $this->clientmdl->fpconnect($key->ipmesin,$key->comkey);
			$mesin = $key->idmesin;
			if ($c == 1) {
				$soap = $this->clientmdl->getlog($key->ipmesin,$key->comkey);
				foreach ($soap as $row) {
                	$siswa = $this->clientmdl->ambilSiswa($row['pin']);
                	if ($siswa) {
                    	$nis = $siswa->nis;
                    } else {
                    	$nis = $row['pin'];
                    }
					$jam = $row['jam'];
					$jns = $row['sts'];
                
					$oks = $this->db->get_where('siswa', array('nis' => $nis))->num_rows();
					if ($oks == 1) {
						$q = "Select * From logfp Where nis = '$nis' And tipe = '$jns' And date(waktu) = date('$jam')";
						$scr = $this->db->query($q);
						# echo $q;
						if ($scr->num_rows() == 0) {
							$dup = array('nis' => $nis, 'tipe' => $jns, 'waktu' => $jam, 'mesin' => $mesin);
							if (!$this->db->insert('logfp', $dup)) {
								log_message('debug',"Gagal mengambil log absen. ".$this->db->error()['message']);
								return false;
							}
						}
					}
                }
                $this->clientmdl->deleteLogMechine($key);
			}
		}
    }
    
    public function uploadPresence() {
		$p = $this->clientmdl->getConfig('url_api');
		$data = array('id_school' => $this->clientmdl->getConfig('sch_code'),
						'key_school' => $this->clientmdl->getConfig('sch_keys'),
						'presence_res' => 'finger',
						'presence' => array());
		$siswa = $this->db->where(array('sync' => '0'))->order_by('waktu desc, tipe asc')->get('logfp', 150, 0)->result();
		$guru = $this->teachermdl->logPresence();
		
		# loop siswa
		foreach ($siswa as $key) {
			$tipe = $this->clientmdl->getPresenceType($key->tipe);

			$wkt = strtotime($key->waktu) - 3600;
			$waktu = date('Y-m-d H:i:s', $wkt);
			$dout = array('id' => $key->nis, 
						'presence_sub' => 'student',
						'presence_type' => $tipe,
						'presence_log' => $waktu);
			array_push($data['presence'],$dout);
		}

		# loop guru
		foreach ($guru as $g) {
			$tipe = $this->clientmdl->getPresenceType($key->tipe);

			$wkt = strtotime($key->waktu) - 3600;
			$waktu = date('Y-m-d H:i:s', $wkt);
			$dout = array('id' => $key->nip, 
						'presence_sub' => 'teacher',
						'presence_type' => $tipe,
						'presence_log' => $waktu);
			array_push($data['presence'],$dout);
		}

		if (count($data['presence']) > 0) {
			$data['presence'] = json_encode($data['presence']);
			log_message('debug',$this->clientmdl->sendLog($data));
		}
	}

	public function deleteLogAllDevice() {
		$res = $this->clientmdl->deleteLogAll();
		log_message('debug', $res);
	}

}
