<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Syncfp extends CI_Controller {

	public function insertprelog() {
		$mesin = $this->db->get('fpmesin')->result();
		foreach ($mesin as $key) {
			$c = $this->clientmdl->fpconnect($key->ipmesin,$key->comkey);
			$mesin = $key->idmesin;
			if ($c == 1) {
				$soap = $this->clientmdl->getlog($key->ipmesin,$key->comkey);
				foreach ($soap as $row) {
                	$siswa = $this->clientmdl->ambilSiswa($row['pin']);
                	if ($siswa) {
                    	$nis = $siswa->nis;
                    } else {
                    	$nis = $row['pin'];
                    }
					$jam = $row['jam'];
					$jns = $row['sts'];
                
					$oks = $this->db->get_where('siswa', array('nis' => $nis))->num_rows();
					if ($oks == 1) {
						$q = "Select * From logfp Where nis = '$nis' And tipe = '$jns' And date(waktu) = date('$jam')";
						$scr = $this->db->query($q);
						# echo $q;
						if ($scr->num_rows() == 0) {
							$dup = array('nis' => $nis, 'tipe' => $jns, 'waktu' => $jam, 'mesin' => $mesin);
							if (!$this->db->insert('logfp', $dup)) {
								echo "Gagal mengambil log absen";
								return false;
							}
						}
					}
				}
				$this->clientmdl->deleteLogMechine($key);
			}
		}
		#$this->clientmdl->deleteLogAll();
		redirect('page/logpresence');
	}

	public function sinkron() {
		$p = $this->clientmdl->getConfig('url_api');
		$data = array('id_school' => $this->clientmdl->getConfig('sch_code'),
						'key_school' => $this->clientmdl->getConfig('sch_keys'),
						'presence_res' => 'finger',
						'presence' => array());

		$rows = $this->db->where(array('sync' => '0'))
				->order_by('waktu desc, tipe asc')->get('logfp', 150, 0)->result();
		# loop for presence
		foreach ($rows as $key) {
			$tipe = $key->tipe;
			$wkt = strtotime($key->waktu) - 3600;
			$waktu = date('Y-m-d H:i:s', $wkt);
			$dout = array('nis' => $key->nis, 
						'presence_type' => 'attend',
						'presence_log' => $waktu);
			array_push($data['presence'],$dout);
			#$data['presence'] = $dout;
		}
		if (count($data['presence']) > 0) {
			$data['presence'] = json_encode($data['presence']);
			log_message('debug',$this->clientmdl->sendLog($data));
		}
		redirect('page/logpresence');
	}

	public function jsondata() {
		$this->authmdl->onlyAdmin();
		echo $this->clientmdl->jsonpresence();
	}

	public function ceklog() {
		$mesin = $this->db->get('fpmesin')->result();
		$out = array();
		foreach ($mesin as $key) {
			$c = $this->clientmdl->fpconnect($key->ipmesin,$key->comkey);
			$mesin = $key->idmesin;
			if ($c == 1) {
				$soap = $this->clientmdl->getlog($key->ipmesin,$key->comkey);
				foreach ($soap as $row) {
                	if ($this->clientmdl->getConfig('idsiswa') == 'nis') {
                		$siswa = $this->clientmdl->siswaByNIS($row['pin']);
            		} else {
                		$siswa = $this->clientmdl->siswaByNOSJ($row['pin']);
            		}
                	if ($siswa) {
                    	$nis = $siswa->nis;
                    } else {
                    	$nis = $row['pin'];
                    }
					$jam = $row['jam'];
					$jns = $row['sts'];
					$o = array('mesin' => $key->ipmesin, 'pin' => $row['pin'], 'nis' => $nis, 'jam' => $jam, 'tipe' => $jns);
                	array_push($out,$o);
				}
			}
		}
		print_r($o);
	}

}
