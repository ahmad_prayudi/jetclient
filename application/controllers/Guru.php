<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Guru extends CI_Controller {
	
	public function datatable() {
		$this->authmdl->onlyAdmin();
		$this->slice->view('guru.table');
	}

	public function jsondata() {
		$this->authmdl->onlyAdmin();
		echo $this->teachermdl->json();
	}

	public function jsonlog() {
		$this->authmdl->onlyAdmin();
		echo $this->teachermdl->jsonlog();
	}

	public function kelola() {
		$this->authmdl->onlyAdmin();
		$data['mesin'] = $this->clientmdl->getallfp();
		$this->slice->view('guru.kelola', $data);
	}

	public function delfp($nip) {
		$this->authmdl->onlyAdmin();
		$data['guru'] = $this->clientmdl->guruBynip($nip);
		$data['mesin'] = $this->db->get('fpmesin')->result();
		$this->slice->view('guru.delfp',$data);
	}

	public function sinkron() {
		#echo "<pre>";
		#print_r($this->teachermdl->synchronize());
		$this->teachermdl->synchronize();
		redirect('guru/datatable');
	}

	public function ritual() {
		$data = $this->input->post();
		$row = $this->mesinmdl->getMesinrow($data['mesin']);
		$con = $this->clientmdl->fpConnect($row->ipmesin, $row->comkey);
		if ($con == 1) {
			$guru = $this->teachermdl->getAllGuru();
			if ($data['mode'] == 0) {
				# download dari mesin
				$ap = array();
				$at = array();
				foreach ($guru as $val) {
					if ($this->clientmdl->getConfig('idguru') == 'nip') {
						$pin = $val->nip;
					} else {
						$pin = $val->pin;
					}
					$this->teachermdl->getpasswordfp($row->ipmesin, $row->comkey, $pin);
					$this->teachermdl->gettemplatefp($row->ipmesin, $row->comkey, $pin);
				}
			} elseif ($data['mode'] == 1) {
				# upload ke mesin
				foreach ($guru as $val) {
					konekid:
					$Connect = @fsockopen($row->ipmesin, '80', $errno, $errstr, 1);
					if ($Connect) {
						$nama = $val->nama;
						if ($this->clientmdl->getConfig('idguru') == 'nip') {
							$pin = $val->nip;
						} else {
							$pin = $val->pin;
						}

						if ($val->pswd == null or $val->pswd == '') {
							$pswd = '';
						} else {
							$pswd = '<Password>'.$val->pswd.'</Password>';
						}
						$soap_request="<SetUserInfo><ArgComKey Xsi:type=\"xsd:integer\">".$row->comkey."</ArgComKey><Arg><PIN>".$pin."</PIN><Name>".$nama."</Name>".$pswd."</Arg></SetUserInfo>";
						$newLine="\r\n";
						@fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
						@fputs($Connect, "Content-Type: text/xml".$newLine);
						@fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
						@fputs($Connect, $soap_request.$newLine);
	                	fclose($Connect);
	                	$this->clientmdl->refreshMechine($row->ipmesin, $row->comkey);
	                	if (count($this->teachermdl->getFPbyNIP($val->nip)) > 0) {
	                		$this->teachermdl->setusertemplatefp($row->ipmesin, $row->comkey, $pin);
	                    }
					} else {
						goto konekid;
					}
				}
				$this->clientmdl->refreshMechine($row->ipmesin, $row->comkey);
			} elseif ($data['mode'] == 2) {
				foreach ($guru as $val) {
					if ($this->clientmdl->getConfig('idguru') == 'nip') {
						$pin = $val->nip;
					} else {
						$pin = $val->pin;
					}
					$this->clientmdl->deleteUserMechine($row->ipmesin, $row->comkey, $pin);
				}
			}
			//Refresh DB
			$Connect = @fsockopen($row->ipmesin, '80', $errno, $errstr, 1);
			$soap_request="<RefreshDB><ArgComKey xsi:type=\"xsd:integer\">".$row->comkey."</ArgComKey></RefreshDB>";
			$newLine="\r\n";
			@fputs($Connect, "POST /iWsService HTTP/1.0".$newLine);
			@fputs($Connect, "Content-Type: text/xml".$newLine);
			@fputs($Connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
			@fputs($Connect, $soap_request.$newLine);

			$this->session->set_flashdata('num', 1);
			$this->session->set_flashdata('msg', 'Data selesai diperbarui');
			redirect('guru/datatable');
		} else {
			echo "koneksi gagal";
		}
	}

}
