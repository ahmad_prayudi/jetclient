<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
@extends('layout')

@section('pagecss')
@parent
<link href="<?=site_url('public')?>/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="<?=site_url('public')?>/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?=site_url('public')?>/plugins/toast/jquery.toast.min.css" rel="stylesheet">
<link href="<?=site_url('public')?>/plugins/bootstrap-date-time-picker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
@endsection

@section('pagehead')
	Log kehadiran mesin
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default collapsed">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-3">
                        <select class="form-control" id="idm" onchange="getLog();">
                            <option value="0">Pilih mesin</option>
                            <?php foreach ($mesin as $m): ?>
                                <?php
                                    if ($idm == $m->idmesin) {
                                        $sel = 'selected';
                                    } else {
                                        $sel = '';
                                    }
                                ?>
                                <option value="<?=$m->idmesin?>" <?=$sel?> ><?=$m->mesin." - ".$m->ipmesin?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <a href="javascript:;" class="btn btn-warning" title="Hapus log mesin" onclick="delLog();">
                            <i class="fa fa-trash"></i> Hapus log
                        </a>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                <table id="datatable" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th style="text-align:center;">PIN</th>
                            <th style="text-align:center;">Nama siswa</th>
                            <th style="text-align:center;">In/Out</th>
                            <th style="text-align:center;">Jam</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (is_array($log)): ?>
                            <?php foreach ($log as $l): 
                                $siswa = $this->clientmdl->ambilSiswa($l['pin']);
                                if ($l['sts'] == '0') {
                                    $sts = 'Masuk';
                                } else {
                                    $sts = 'Keluar';
                                }
                                $jam = date('d-m-Y H:i:s', strtotime($l['jam']));
                                if ($siswa):
                            ?>
                                <tr>
                                    <td><?=$l['pin']?></td>
                                    <td><?=$siswa->nama?></td>
                                    <td><?=$sts?></td>
                                    <td><?=$jam?></td>
                                </tr>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td class="txt">PIN</td>
                            <td class="txt">nama</td>
                            <td class="cmb">status</td>
                            <td class="txt">tgl/jam</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('pagejs')
@parent
<script src="<?=site_url('public')?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=site_url('public')?>/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?=site_url('public')?>/plugins/toast/jquery.toast.min.js"></script>
<script src="<?=site_url('public')?>/plugins/bootstrap-date-time-picker/js/bootstrap-datetimepicker.min.js"></script>
<script>
    $(document).ready(function () {
        var dtab = null;
        $('.txt').each( function () {
            var title = $(this).text();
            $(this).html( '<input class="form-control" type="search" placeholder="Search '+title+'" />' );
        });

        // add select masuk/keluar
        var combro = '<select class="form-control">';
        combro += '<option value="">Semua status</option>';
        combro += '<option value="Masuk">Status masuk</option>';
        combro += '<option value="Keluar">Status keluar</option>';
        combro += '</select>';
        $('.cmb').html(combro);

        dtab = $('#datatable').DataTable({
            "order": [[3, 'desc']],
            "scrollY": "200px",
            "columnDefs": [
                { "width": "15%", "targets": 0 },
                { "width": "40%", "targets": 1 },
                { "width": "15%", "targets": 2 },
                { "width": "15%", "targets": 3 }
            ],
            "lengthMenu": [[10, 15, 20, 50, -1], 
                            [10+" Siswa", 15+" Siswa", 20+" Siswa", 50+" Siswa", "Semua Siswa"]]
        });
        // Apply the search
        dtab.columns().every( function () {
            var that = this;
            $( 'select', this.footer() ).on( 'change', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            });
        });
        dtab.columns().every( function () {
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            });
        });
    });

    function getLog() {
        $(function() {
            var m  = $('#idm').val();
            window.location = "<?=base_url('page/logmechine')?>/"+m;
        });
    }

    function delLog() {
        $(function() {
            var m  = $('#idm').val();
            if (m != 0) {
                window.location = "<?=base_url('client/deletelogmechine')?>/"+m;
            } else {
                alert('Belum ada mesin yang dipilih...');
            }
        });
    }
</script>
@endsection