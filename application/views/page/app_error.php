<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>JetClient - Application Error</title>

        <!-- Common plugins -->
        <link href="<?=site_url('public')?>/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?=site_url('public')?>/plugins/simple-line-icons/simple-line-icons.css" rel="stylesheet">
        <link href="<?=site_url('public')?>/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?=site_url('public')?>/plugins/pace/pace.css" rel="stylesheet">
        <link href="<?=site_url('public')?>/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?=site_url('public')?>/plugins/nano-scroll/nanoscroller.css">
        <link rel="stylesheet" href="<?=site_url('public')?>/plugins/metisMenu/metisMenu.min.css">
        <!--template css-->
        <link href="<?=site_url('public')?>/css/style.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="<?=site_url('public')?>/https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="<?=site_url('public')?>/https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            html,body{
                height: 100%;
            }
        </style>
    </head>
    <body>

        <div class="misc-wrapper">
            <div class="misc-content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                            <div class="misc-box text-center">
                                <h1 class="text-muted">Application error.</h1>
                                <h4 class="font-300">Ada kesalahan ketiak mengakses aplikasi, seilahkan coba lagi atau hubungi admin aplikasi</h4>
                                <a href="<?=site_url('welcome')?>" class="btn btn-lg btn-default btn-rounded btn-xs">Ke beranda</a>
                            </div>
                            <div class="text-center misc-footer">
                                <span>&copy; Copyright <?=date('Y')?>. Jetschool</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--Common plugins-->
        <script src="<?=site_url('public')?>/plugins/jquery/dist/jquery.min.js"></script>
        <script src="<?=site_url('public')?>/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?=site_url('public')?>/plugins/pace/pace.min.js"></script>
        <script src="<?=site_url('public')?>/plugins/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
        <script src="<?=site_url('public')?>/plugins/slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?=site_url('public')?>/plugins/nano-scroll/jquery.nanoscroller.min.js"></script>
        <script src="<?=site_url('public')?>/plugins/metisMenu/metisMenu.min.js"></script>
        <script src="<?=site_url('public')?>/js/float-custom.js"></script>
    </body>
</html>
