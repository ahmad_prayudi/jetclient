<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
@extends('layout')

@section('pagehead')
	Konfigurasi Aplikasi Client
@endsection

@section('pagecss')
	<link href="<?=site_url('public')?>/plugins/bootstrap-date-time-picker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
@endsection

@section('pagejs')
	<script src="<?=site_url('public')?>/plugins/bootstrap-date-time-picker/js/bootstrap-datetimepicker.min.js"></script>
	<script>
        $(function () {
            $('#autodel').datetimepicker({
                format: 'HH:mm'
            });
        });
    </script>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
			<div class="panel-body">
                <?php if ($this->session->flashdata('msg')): ?>
                <div class="alert alert-success alert-dismissible fade in margin-b-0" role="alert"> 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button> 
                    <?=$this->session->flashdata('msg')?> 
                </div>
                <?php endif; ?>
                <br>
            	<form action="<?=site_url('client/updateconf')?>" method="post" class="form-horizontal">
					<fieldset>
                        <div class="form-group">
                            <label for="url_api" class="col-sm-2 control-label">URL Aplikasi</label>
                            <div class="col-sm-10">
                                <input id="url_api" name="url_api" type="text" value="<?=$url_api?>" class="form-control">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label for="sch_code" class="col-sm-2 control-label">ID School</label>
                            <div class="col-sm-10">
                                <input id="sch_code" name="sch_code" type="text" value="<?=$sch_code?>" class="form-control">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label for="sch_keys" class="col-sm-2 control-label">Key School</label>
                            <div class="col-sm-10">
                                <input id="sch_keys" name="sch_keys" type="text" value="<?=$sch_keys?>" class="form-control">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label for="sch_keys" class="col-sm-2 control-label">Username admin</label>
                            <div class="col-sm-10">
                                <input id="username" name="username" type="text" value="<?=$username?>" class="form-control">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label for="sch_keys" class="col-sm-2 control-label">Password admin</label>
                            <div class="col-sm-10">
                                <input id="usercode" name="usercode" type="password" value="<?=$usercode?>" class="form-control">
                                <i><?=$u?></i>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label for="sch_keys" class="col-sm-2 control-label">Identitas Guru</label>
                            <div class="col-sm-10">
                                <select name="idguru" class="form-control">
                                    <?php
                                        if ($idguru == 'nip') {
                                            $snis = 'selected';
                                        } else {
                                            $snis = '';
                                        }
                                    ?>
                                    <option value="pin">PIN</option>
                                    <option value="nip" <?=$snis?>>NIP</option>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label for="sch_keys" class="col-sm-2 control-label">Identitas Siswa</label>
                            <div class="col-sm-10">
                                <select name="idsiswa" class="form-control">
                                    <?php
                                        if ($idsiswa == 'nis') {
                                            $snis = 'selected';
                                        } else {
                                            $snis = '';
                                        }
                                    ?>
                                    <option value="nosj">PIN</option>
                                    <option value="nis" <?=$snis?>>NIS</option>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label for="sch_keys" class="col-sm-2 control-label">Hapus log mesin</label>
                            <div class="col-sm-4">
                            	<div class='input-group date' id='autodel'>
                                    <input type='text' class="form-control" name="autodel_log" value="<?=$autodel_log?>"  />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                            	Klik tombol <span class="glyphicon glyphicon-time"></span> untuk mengatur jam
                            </div>
                        </div>
                    </fieldset>
                    <button type="submit" class="btn btn-sm btn-primary">Simpan</button>
            	</form>
            </div>
        </div>
    </div>
</div>
@endsection
