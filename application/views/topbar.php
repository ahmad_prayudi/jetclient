<!--top bar start-->
<div class="top-bar light-top-bar">
    
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-6">
                <a href="#" class="admin-logo">
                    <h1 style="color: black;">JetClient</h1>
                </a>
                <div class="left-nav-toggle visible-xs visible-sm">
                    <a href="<?=site_url('public')?>/">
                        <i class="glyphicon glyphicon-menu-hamburger"></i>
                    </a>
                </div><!--end nav toggle icon-->
            </div>
        </div>
    </div>
</div>
<!-- top bar end-->