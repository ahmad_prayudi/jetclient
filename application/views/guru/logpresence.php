<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
@extends('layout')

@section('pagecss')
@parent
<link href="<?=site_url('public')?>/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="<?=site_url('public')?>/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?=site_url('public')?>/plugins/toast/jquery.toast.min.css" rel="stylesheet">
@endsection

@section('pagehead')
	Kehadiran guru
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default collapsed">
			<div class="panel-body">
				<table id="datatable" class="table table-striped dt-responsive nowrap">
					<thead>
						<tr>
							<th style="text-align:center;">NIP</th>
							<th style="text-align:center;">Nama guru</th>
							<th style="text-align:center;">Tgl/Jam</th>
							<th style="text-align:center;">Mesin FP</th>
							<th style="text-align:center;">#</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@section('pagejs')
@parent
<script src="<?=site_url('public')?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=site_url('public')?>/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?=site_url('public')?>/plugins/toast/jquery.toast.min.js"></script>
<script>
	$(document).ready(function () {
		var dtab = $('#datatable').dataTable({
			"processing": true,
			"serverSide": true,
			"order": [[2, 'desc']],
			"scrollY": "200px",
			"columnDefs": [
				{ "width": "10%", "targets": 0 },
				{ "width": "40%", "targets": 1 },
				{ "width": "15%", "targets": 2 },
				{ "width": "15%", "orderable": false, "targets": 3 },
				{ "width": "15%", "orderable": false, "targets": 4 }
			],
			"lengthMenu": [[25, 50, 100, 200, -1], 
							[25+" guru", 50+" guru", 100+" guru", 200+" guru", "Semua guru"]],
			"sAjaxSource": "<?=site_url('guru/jsonlog')?>"
		});

		<?php if ($this->session->flashdata('num') == '0'): ?>
		$.toast({
			heading: 'Koneksi gagal',
			text: "<?=$this->session->flashdata('msg')?>",
			position: 'top-right',
			loaderBg: '#fff',
			icon: 'warning',
			hideAfter: 3000,
			stack: 1
		});
		<?php elseif ($this->session->flashdata('num') == '1'): ?>
		$.toast({
			heading: 'Koneksi sukses',
			text: "<?=$this->session->flashdata('msg')?>",
			position: 'top-right',
			loaderBg: '#fff',
			icon: 'success',
			hideAfter: 3000,
			stack: 1
		});
		<?php endif; ?>
	});
</script>
@endsection
