<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
@extends('layout')

@section('pagecss')
@parent
<link href="<?=site_url('public')?>/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="<?=site_url('public')?>/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?=site_url('public')?>/plugins/toast/jquery.toast.min.css" rel="stylesheet">
@endsection

@section('pagehead')
	Tabel data guru
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default collapsed">
			<div class="panel-heading">
				<a href="<?=site_url('guru/sinkron')?>" class="btn btn-sm btn-primary">
					<span class="fa fa-database"> Sinkron data
				</a>
				<a href="<?=site_url('guru/kelola')?>" class="btn btn-sm btn-primary">
					<span class="fa fa-exchange"> Update data
				</a>
				<?php
					if ($this->session->flashdata('msg')) {
						//print_r($this->session->flashdata('msg'));
						echo '<i>'.$this->session->flashdata('msg').'</i>';
					}
				?>
			</div>

			<div class="panel-body">
				<table id="datatable" class="table table-striped dt-responsive nowrap">
					<thead>
						<tr>
							<th style="text-align:center;">NIP</th>
							<th style="text-align:center;">PIN</th>
							<th style="text-align:center;">Nama guru</th>
							<th style="text-align:center;">Verifikasi</th>
						</tr>
					</thead>
				</table>
				<?php
					if ($this->session->flashdata('errors')) {
						echo 'Daftar guru gagal sinkron.<br>';
						echo '<i>'.$this->session->flashdata('errors').'</i>';
					}
				?>
			</div>
		</div>
	</div>
</div>
@endsection

@section('pagejs')
@parent
<script src="<?=site_url('public')?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=site_url('public')?>/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?=site_url('public')?>/plugins/toast/jquery.toast.min.js"></script>
<script>
	$(document).ready(function () {
		var dtab = $('#datatable').dataTable({
			"processing": true,
			"serverSide": true,
			"order": [[2, 'asc']],
			"scrollY": "225px",
			"autoWidth": false,
			"columnDefs": [
				{ "width": "100px", "targets": 0 },
				{ "width": "100px", "targets": 1 },
				{ "width": "150px", "orderable": false, "targets": 3 }
			],
			"lengthMenu": [[15, 30, 60, -1], 
							[15+" guru", 30+" guru", 60+" guru", "Semua guru"]],
			"sAjaxSource": "<?=site_url('guru/jsondata')?>"
		});

		<?php if ($this->session->flashdata('num') == '0'): ?>
		$.toast({
			heading: 'Koneksi gagal',
			text: "<?=$this->session->flashdata('msg')?>",
			position: 'top-right',
			loaderBg: '#fff',
			icon: 'warning',
			hideAfter: 3000,
			stack: 1
		});
		<?php elseif ($this->session->flashdata('num') == '1'): ?>
		$.toast({
			heading: 'Koneksi sukses',
			text: "<?=$this->session->flashdata('msg')?>",
			position: 'top-right',
			loaderBg: '#fff',
			icon: 'success',
			hideAfter: 3000,
			stack: 1
		});
		<?php endif; ?>
	});
</script>
@endsection
