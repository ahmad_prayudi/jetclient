<!--left navigation start-->
<aside class="float-navigation light-navigation">
    <div class="nano">
        <div class="nano-content">
            <ul class="metisMenu nav" id="menu">
                <li>
                    <a href="<?=site_url('welcome')?>">
                        <i class="fa fa-dashboard"></i> Beranda 
                    </a>
                </li>
                <li class="nav-heading"><span>Konfigurasi</span></li>
                <li>
                    <a href="<?=site_url('page/aplikasi')?>">
                        <i class="fa fa-database"></i> Kelola aplikasi 
                    </a>
                </li>
                <li>
                    <a href="<?=site_url('mesin/datatable')?>">
                        <i class="fa fa-desktop"></i> Data mesin 
                    </a>
                </li>
                
                <li class="nav-heading"><span>Presensi</span></li>
                <li>
                    <a href="<?=site_url('guru/datatable')?>">
                        <i class="fa fa-users"></i> Data guru
                    </a>
                </li>
                <li>
                    <a href="<?=site_url('siswa/datatable')?>">
                        <i class="fa fa-graduation-cap"></i> Data siswa
                    </a>
                </li>

                <li>
                    <a href="<?=site_url('page/guruatt')?>">
                        <i class="fa fa-institution"></i> Kehadiran guru
                    </a>
                </li>
                <li>
                    <a href="<?=site_url('page/logpresence')?>">
                        <i class="fa fa-bell"></i> Kehadiran siswa
                    </a>
                </li>
                <li>
                    <a href="<?=site_url('page/logmechine')?>">
                        <i class="fa fa-tasks"></i> Log mesin
                    </a>
                </li>

                <li class="nav-heading"><span>Akun</span></li>
                <li>
                    <a href="<?=site_url('auth/signout')?>">
                        <i class="fa fa-sign-out"></i> Keluar
                    </a>
                </li>
                
            </ul>
        </div><!--nano content-->
    </div><!--nano scroll end-->
</aside>
<!--left navigation end-->
