<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Jetclient</title>
        <!-- Common plugins -->
        <link href="<?=site_url('public')?>/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?=site_url('public')?>/plugins/simple-line-icons/simple-line-icons.css" rel="stylesheet">
        <link href="<?=site_url('public')?>/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?=site_url('public')?>/plugins/pace/pace.css" rel="stylesheet">
        <link href="<?=site_url('public')?>/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?=site_url('public')?>/plugins/nano-scroll/nanoscroller.css">
        <link rel="stylesheet" href="<?=site_url('public')?>/plugins/metisMenu/metisMenu.min.css">
        <link href="<?=site_url('public')?>/plugins/chart-c3/c3.min.css" rel="stylesheet">
        <link href="<?=site_url('public')?>/plugins/iCheck/blue.css" rel="stylesheet">
        
        @section('pagecss')
        <!-- page style -->
        @show
        <!--template css-->
        <link href="<?=site_url('public')?>/css/style.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="<?=site_url('public')?>/https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="<?=site_url('public')?>/https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        @include('topbar')

        @include('leftbar')


        <!--main content start-->
        <section class="main-content">

            <!--page header start-->
            <div class="page-header">
                <div class="row">
                    <div class="col-sm-6">
                        <h4>@yield('pagehead')</h4>
                    </div>
                </div>
            </div>
            <!--page header end-->


            <!--start page content-->
            @yield('content')
            <!--end page content-->


            <!--Start footer-->
            <footer class="footer">
                <span>Copyright &copy; <?=date('Y')?>. JetSchool</span>
            </footer>
            <!--end footer-->

        </section>
        <!--end main content-->



        <!--Common plugins-->
        <script src="<?=site_url('public')?>/plugins/jquery/dist/jquery.min.js"></script>
        <script src="<?=site_url('public')?>/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?=site_url('public')?>/plugins/pace/pace.min.js"></script>
        <script src="<?=site_url('public')?>/plugins/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
        <script src="<?=site_url('public')?>/plugins/slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?=site_url('public')?>/plugins/nano-scroll/jquery.nanoscroller.min.js"></script>
        <script src="<?=site_url('public')?>/plugins/metisMenu/metisMenu.min.js"></script>
        <script src="<?=site_url('public')?>/js/float-custom.js"></script>
        <script src="<?=site_url('public')?>/plugins/momentJs/moment-with-locales.js"></script>

        @section('pagejs')
        <!--page script-->
        @show
        <script src="<?=site_url('public')?>/js/jetclient.js"></script>
    </body>
</html>