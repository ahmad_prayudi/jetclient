<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>JetClient - Login</title>

        <!-- Common plugins -->
        <link href="<?=site_url('public')?>/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?=site_url('public')?>/plugins/simple-line-icons/simple-line-icons.css" rel="stylesheet">
        <link href="<?=site_url('public')?>/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?=site_url('public')?>/plugins/pace/pace.css" rel="stylesheet">
        <link href="<?=site_url('public')?>/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?=site_url('public')?>/plugins/nano-scroll/nanoscroller.css">
        <link rel="stylesheet" href="<?=site_url('public')?>/plugins/metisMenu/metisMenu.min.css">
        <!--for checkbox-->
        <link href="<?=site_url('public')?>/plugins/iCheck/blue.css" rel="stylesheet">
        <!--template css-->
        <link href="<?=site_url('public')?>/css/style.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="<?=site_url('public')?>/https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="<?=site_url('public')?>/https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            html,body{
                height: 100%;
            }
        </style>
    </head>
    <body>

        <div class="misc-wrapper">
            <div class="misc-content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                            <div class="misc-header text-center">
                                <img src="<?=site_url('public')?>/images/logo-default.png" alt="">
                            </div>
                            <div class="misc-box">   
                                <p class="text-center text-uppercase pad-v">Login to continue.</p>
                                <p class="text-center text-uppercase pad-v"><?=$this->session->flashdata('errlog')?></p>
                                <form role="form" action="<?=site_url('auth/signin')?>" method="post">
                                    <div class="form-group">                                      
                                        <label class="text-muted" for="username">Username</label>
                                        <div class="group-icon">
                                        <input id="username" name="username" type="text" placeholder="Username" class="form-control" required="">
                                        <span class="icon-user text-muted icon-input"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-muted" for="usercode">Password</label>
                                        <div class="group-icon">
                                        <input id="usercode" name="usercode" type="password" placeholder="Password admin" class="form-control">
                                        <span class="icon-lock text-muted icon-input"></span>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="pull-right">
                                            <button type="submit" class="btn btn-block btn-primary">Login</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="text-center misc-footer">
                                <span>&copy; Copyright <?=date('Y')?>. JetSchool</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--Common plugins-->
        <script src="<?=site_url('public')?>/plugins/jquery/dist/jquery.min.js"></script>
        <script src="<?=site_url('public')?>/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?=site_url('public')?>/plugins/pace/pace.min.js"></script>
        <script src="<?=site_url('public')?>/plugins/jasny-bootstrap/js/jasny-bootstrap.min.js"></script>
        <script src="<?=site_url('public')?>/plugins/slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?=site_url('public')?>/plugins/nano-scroll/jquery.nanoscroller.min.js"></script>
        <script src="<?=site_url('public')?>/plugins/metisMenu/metisMenu.min.js"></script>
        <script src="<?=site_url('public')?>/js/float-custom.js"></script>
        <!--ichecks-->
        <script src="<?=site_url('public')?>/plugins/iCheck/icheck.min.js"></script>
        <script>
            $(document).ready(function () {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue'
                });
            });
        </script>
    </body>
</html>
