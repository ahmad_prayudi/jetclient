<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
@extends('layout')

@section('pagecss')
@parent
<link href="<?=site_url('public')?>/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="<?=site_url('public')?>/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?=site_url('public')?>/plugins/toast/jquery.toast.min.css" rel="stylesheet">
@endsection

@section('pagehead')
	Tabel data mesin
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default collapsed">
            <div class="panel-heading">
                <a href="<?=site_url('mesin/addfpm')?>" class="btn btn-sm btn-primary">
                    <span class="fa fa-plus"> Tambah mesin
                </a>
            </div>

            <div class="panel-body">
                <table id="datatable" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th style="text-align:center;">Nama mesin</th>
                            <th style="text-align:center;">IP mesin</th>
                            <th style="text-align:center;">Comkey</th>
                            <th style="text-align:center;">Status</th>
                            <th style="text-align:center;"><span class="fa fa-sliders"></th>
                        </tr>
                    </thead>
                    <?=$tabel?>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('pagejs')
@parent
<script src="<?=site_url('public')?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=site_url('public')?>/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?=site_url('public')?>/plugins/toast/jquery.toast.min.js"></script>
<script>
    $(document).ready(function () {
        $('#datatable').dataTable();

        <?php if ($this->session->flashdata('num') == '0'): ?>
        $.toast({
            heading: 'Koneksi gagal',
            text: "<?=$this->session->flashdata('msg')?>",
            position: 'top-right',
            loaderBg: '#fff',
            icon: 'warning',
            hideAfter: 3000,
            stack: 1
        });

        <?php elseif ($this->session->flashdata('num') == '1'): ?>
        $.toast({
            heading: 'Koneksi sukses',
            text: "<?=$this->session->flashdata('msg')?>",
            position: 'top-right',
            loaderBg: '#fff',
            icon: 'success',
            hideAfter: 3000,
            stack: 1
        });
        <?php endif; ?>

        setInterval(function() { getRealFM(); }, 60000);
    });
</script>
@endsection