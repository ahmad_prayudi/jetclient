<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
@extends('layout')

@section('pagehead')
	Form data mesin
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
			<div class="panel-body">
                <?php if ($this->session->flashdata('msg')): ?>
                <div class="alert alert-success alert-dismissible fade in margin-b-0" role="alert"> 
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button> 
                    <?=$this->session->flashdata('msg')?> 
                </div>
                <?php endif; ?>
                <br>
            	<form action="<?=site_url('client/mesin/'.$mode.'/'.$idmesin)?>" method="post" class="form-horizontal">
					<fieldset>
                        <div class="form-group">
                            <label for="mesin" class="col-sm-2 control-label">Nama mesin</label>
                            <div class="col-sm-10">
                                <input id="mesin" name="mesin" type="text" value="<?=$mesin?>" class="form-control">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label for="ipmesin" class="col-sm-2 control-label">IP mesin</label>
                            <div class="col-sm-10">
                                <input id="ipmesin" name="ipmesin" type="text" value="<?=$ipmesin?>" class="form-control">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-group">
                            <label for="comkey" class="col-sm-2 control-label">Comkey mesin</label>
                            <div class="col-sm-10">
                                <input id="comkey" name="comkey" type="text" value="<?=$comkey?>" class="form-control">
                            </div>
                        </div>
                    </fieldset>
                    <button type="submit" class="btn btn-sm btn-primary">Simpan</button>
            	</form>
            </div>
        </div>
    </div>
</div>
@endsection