<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
@extends('layout')

@section('pagecss')
@parent
<link href="<?=site_url('public')?>/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="<?=site_url('public')?>/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?=site_url('public')?>/plugins/toast/jquery.toast.min.css" rel="stylesheet">
@endsection

@section('pagehead')
	Tabel data siswa
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default collapsed">
            <div class="panel-heading">
                <a href="<?=site_url('siswa/sinkron')?>" class="btn btn-sm btn-primary">
                    <span class="fa fa-database"> Sinkron data
                </a>
                <a href="<?=site_url('siswa/kelola')?>" class="btn btn-sm btn-primary">
                    <span class="fa fa-exchange"> Update data
                </a>
            </div>

            <div class="panel-body">
                <table id="datatable" class="table table-striped dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th style="text-align:center;">NIS</th>
                            <th style="text-align:center;">PIN</th>
                            <th style="text-align:center;">Nama siswa</th>
                            <th style="text-align:center;">Sidik jari</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('pagejs')
@parent
<script src="<?=site_url('public')?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=site_url('public')?>/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?=site_url('public')?>/plugins/toast/jquery.toast.min.js"></script>
<script>
    $(document).ready(function () {
        var dtab = $('#datatable').dataTable({
            "processing": true,
            "serverSide": true,
            "order": [[2, 'asc']],
            "scrollY": "225px",
            "autoWidth": false,
            "columnDefs": [
                { "width": "100px", "targets": 0 },
                { "width": "100px", "targets": 1 },
                { "width": "50px", "targets": 3 }
            ],
            "lengthMenu": [[15, 30, 60, -1], 
                            [15+" Siswa", 30+" Siswa", 60+" Siswa", "Semua Siswa"]],
            "sAjaxSource": "<?=site_url('siswa/jsondata')?>"
        });

        <?php if ($this->session->flashdata('num') == '0'): ?>
        $.toast({
            heading: 'Koneksi gagal',
            text: "<?=$this->session->flashdata('msg')?>",
            position: 'top-right',
            loaderBg: '#fff',
            icon: 'warning',
            hideAfter: 3000,
            stack: 1
        });
        <?php elseif ($this->session->flashdata('num') == '1'): ?>
        $.toast({
            heading: 'Koneksi sukses',
            text: "<?=$this->session->flashdata('msg')?>",
            position: 'top-right',
            loaderBg: '#fff',
            icon: 'success',
            hideAfter: 3000,
            stack: 1
        });
        <?php endif; ?>
    });
</script>
@endsection
