<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
@extends('layout')

@section('pagecss')
@parent
<style>
    select {
        font-family: FontAwesome, sans-serif;
    }
</style>
@endsection

@section('pagehead')
	Download/Upload Siswa - Mesin FP
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
			<div class="panel-body">
                <form action="<?=site_url('siswa/ritual')?>" method="post" class="form-horizontal">
					<fieldset>
                        <div class="form-group">
                            <label for="mode" class="col-sm-2 control-label">Mode Proses</label>
                            <div class="col-sm-10">
                                <select name="mode" class="form-control m-b">
                                    <option value="0">&#xf019; Download jari dan password</option>
                                    <option value="1">&#xf093; Upload seluruh data siswa</option>
                                    <option value="2">&#xf1f8; Hapus seluruh data siswa</option>
                                </select>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset>
                        <div class="form-group">
                            <label for="mesin" class="col-sm-2 control-label">Mesin tujuan</label>
                            <div class="col-sm-10">
                                <select name="mesin" class="form-control m-b">
                                    <?php
                                        foreach ($mesin as $value) {
                                            $idm = $value->idmesin;
                                            $nmm = $value->mesin;
                                            echo "<option value='$idm' $sel>$nmm</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </fieldset>

                    <button type="submit" class="btn btn-sm btn-primary">Proses</button>
            	</form>
            </div>
        </div>
    </div>
</div>
@endsection