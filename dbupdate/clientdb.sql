
DROP TABLE IF EXISTS `guru`;
CREATE TABLE `guru`  (
  `nip` varchar(9) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nama` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pin` varchar(6) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pswd` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`nip`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

DROP TABLE IF EXISTS `jariguru`;
CREATE TABLE `jariguru`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nomor` int(10) UNSIGNED NULL DEFAULT NULL,
  `ukuran` int(10) NULL DEFAULT NULL,
  `tmplate` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `nip` varchar(9) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

DROP TABLE IF EXISTS `logfpguru`;
CREATE TABLE `logfpguru`  (
  `idlog` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nip` varchar(16) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pin` varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tipe` enum('0','1','4','5') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '0 = masuk, 1 = pulang',
  `waktu` timestamp(0) NULL DEFAULT NULL,
  `sync` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mesin` int(10) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`idlog`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

DROP TABLE IF EXISTS `uploaded`;
CREATE TABLE `uploaded` (
  `id` int(10) UNSIGNED NOT NULL,
  `mesin_sn` varchar(30) NOT NULL,
  `client_ip` varchar(16) DEFAULT NULL,
  `reqtype` varchar(20) DEFAULT NULL,
  `uploaded` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `parameter` text,
  `timestamps` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `uploaded`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `uploaded`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
